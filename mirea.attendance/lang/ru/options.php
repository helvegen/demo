<?php

$MESS['MIREA_ATTENDANCE_FORBIDDEN'] = 'Нет доступа';
$MESS['MIREA_ATTENDANCE_SCUD_SETTINGS'] = 'Параметры СКУДа';
$MESS['MIREA_ATTENDANCE_API_KEY_LABEL'] = 'Ключ доступа';
$MESS['MIREA_ATTENDANCE_HOST_LABEL'] = 'Хост';
$MESS['MIREA_ATTENDANCE_PORT_LABEL'] = 'Порт';
$MESS['MIREA_ATTENDANCE_INTERVAL_LABEL'] = 'Интервал запуска (в часах)';
$MESS['MIREA_ATTENDANCE_REQUIRED_NOTE'] = 'Значения полей, отмеченных знаком *, нельзя сохранить пустыми.';
$MESS['MIREA_ATTENDANCE_SHOW_SUBS_LABEL'] = 'Показывать посещения подчиненных в отделе';
