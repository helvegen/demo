<?php

namespace Mirea\Attendance;

use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use CUser;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;

class ScudData
{
    private $module_options;

    public const DATE_FORMAT = 'Ymd';
    public const PATH_MASK = 'http://##HOST##:##PORT##/api/##METHOD##/##KEY##/##START##/##END##';
    public const MODULE_ID = 'mirea.attendance';
    public const MODULE_PREFIX = 'mirea_attendance';

    public function __construct()
    {
        $arOpt = Option::getForModule(self::MODULE_ID);
        foreach ($arOpt as $k => $v) {
            $key = str_replace(self::MODULE_PREFIX . '_', '', $k);
            $this->module_options[$key] = $v;
        }
    }

    public static function AddOrUpdateAgent()
    {
        Loader::includeModule(self::MODULE_ID);

        $obScud = new self();
        $arEmp = $obScud->ScudConnect();

        // find bitrix users
        $arUsers = $obScud->FindUser(array_column($arEmp, '@id_staff_external'));

        // add/update
        $arReturn = array();
        foreach ($arEmp as $userData) {
            $extID = trim($userData['@id_staff_external'], '{}');
            if ($userData['events']['event']) {
                if ($userData['events']['event'][0] and is_array($userData['events']['event'][0])) {
                    // add entrance
                    $obStart = new DateTime($userData['events']['event'][0]['@datetimeevent']);
                    if (!$obScud->GetEventByDate($obStart, $extID)) {
                        $rowID = $obScud->GetLastID() + 1; // костыль
                        $arFields = array(
                            'id' => $rowID,
                            'datetime' => $obStart,
                            'user_ext_id' => $extID,
                            'user_fio' => $userData['@fio'],
                            'user_id' => $arUsers[$extID] ? $arUsers[$extID] : 0
                        );
                        $res = AttendanceTable::add($arFields);
                        if ($res->isSuccess()) {
                            $arReturn[] = $rowID;
                        }
                    }
                }

                // add or update exit
                $ar = end($userData['events']['event']);
                if ($ar and is_array($ar) and $ar !== $userData['events']['event'][0]) {
                    if ($lastExit = $obScud->GetLastExit($extID)) {
                        if ($lastExit['datetime'] != new DateTime($ar['@datetimeevent'])) {
                            $arFields = array('datetime' => new DateTime($ar['@datetimeevent']));
                            $res = AttendanceTable::update($lastExit['id'], $arFields);
                            if ($res->isSuccess()) {
                                $arReturn[] = $lastExit['id'];
                            }
                        }
                    } else {
                        $rowID = $obScud->GetLastID() + 1; // костыль
                        $arFields = array(
                            'id' => $rowID,
                            'datetime' => new DateTime($ar['@datetimeevent']),
                            'user_ext_id' => $extID,
                            'user_fio' => $userData['@fio'],
                            'user_id' => $arUsers[$extID] ? $arUsers[$extID] : 0
                        );
                        $res = AttendanceTable::add($arFields);
                        if ($res->isSuccess()) {
                            $arReturn[] = $rowID;
                        }
                    }
                }
            }
        }

        return '\Mirea\Attendance\ScudData::AddOrUpdateAgent();';
    }

    // TODO: отрефакторить производительность
    public static function DeleteOldDataAgent()
    {
        Loader::includeModule(self::MODULE_ID);

        // get old id
        $obDate = new DateTime(
            date('d.m.Y', strtotime('-6 month'))
        );
        $res = AttendanceTable::getList(
            array(
                'filter' => array(
                    '<=datetime' => $obDate
                ),
                'select' => array('id')
            )
        )->fetchAll();
        // delete old data
        if($res) {
            foreach (array_column($res, 'id') as $v) {
                AttendanceTable::delete($v);
            }
        }

        // renew rest id
        AttendanceTable::resetID();

        return '\Mirea\Attendance\ScudData::DeleteOldDataAgent();';
    }

    public static function GetUserData($arParams)
    {
        $arReturn = array();
        $res = AttendanceTable::getList(
            array(
                'filter' => array(
                    'user_ext_id' => $arParams['EXT_ID'],
                    '>=datetime' => new DateTime($arParams['START']),
                    '<=datetime' => new DateTime($arParams['END'])
                ),
                'order' => array(
                    'datetime' => 'desc'
                ),
            )
        )->fetchAll();
        $arReturn['USER'] = array(
            'ID' => end($res)['user_id'],
            'FULLNAME' => end($res)['user_fio'],
            'EXT_ID' => end($res)['user_ext_id']
        );
        $arEvents = array_combine(array_column($res, 'id'), array_column($res, 'datetime'));
        $arReturn['EVENTS'] = array_map(
            function ($obBXDate) {
                $ob = new DateTime($obBXDate);
                return $ob->getTimestamp();
            },
            $arEvents
        );
        return $arReturn;
    }

    // костыль
    public function GetLastID()
    {
        $connection = Application::getConnection();
        $query = "SELECT MAX(id) FROM " . AttendanceTable::getTableName();
        $res = $connection->query($query);
        return $res->fetch()['MAX(id)'];
    }

    public function GetLastExit($extID)
    {
        $arReturn = array();
        $arFilter = array(
            'user_ext_id' => $extID,
            '>=datetime' => new DateTime(date('d.m.Y ') . ' 00:00:00')
        );
        $res = AttendanceTable::getList(
            array('filter' => $arFilter)
        )->fetchAll();
        if (count($res) > 1) {
            $arReturn = end($res);
        }

        return $arReturn;
    }

    public function FindUser($arExtID)
    {
        $arReturn = array();
        foreach ($arExtID as $sExtID) {
            $sExtID = trim($sExtID, '{}');
            $res = CUser::GetList(
                $by = 'id',
                $order = 'asc',
                array('XML_ID' => $sExtID)
            );
            while ($ar = $res->GetNext()) {
                $arReturn[$sExtID] = $ar['ID'];
            }
        }

        return $arReturn;
    }

    public function GetEventByDate(DateTime $dateTime, string $extID)
    {
        return AttendanceTable::getList(
            array(
                'filter' => array(
                    '=datetime' => $dateTime,
                    'user_ext_id' => $extID
                )
            )
        )->fetchAll();
    }

    private function ScudConnect()
    {
        $sPath = str_replace(
            array('##HOST##', '##PORT##', '##METHOD##', '##KEY##', '##START##', '##END##'),
            array(
                $this->module_options['host'],
                $this->module_options['port'],
                'getemployeeevents',
                $this->module_options['api_key'],
                date(self::DATE_FORMAT),
                date(self::DATE_FORMAT)
            ),
            self::PATH_MASK
        );
        $jRes = file_get_contents($sPath);
        $arRes = json_decode($jRes, true);
        return $arRes['eventsreport']['employees']['employ'];
    }

}
