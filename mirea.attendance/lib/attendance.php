<?php

namespace Mirea\Attendance;

use Bitrix\Main\Application;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\DatetimeField;
use Bitrix\Main\ORM\Fields\StringField;

class AttendanceTable extends DataManager
{
    public static function getTableName()
    {
        return 'mirea_attendance_table';
    }

    public static function getMap()
    {
        return array(
            new IntegerField(
                'id',
                array(
                    'primary' => true,
                    'autocomplete' => true
                )
            ),
            new DatetimeField(
                'datetime',
                array(
                    'required' => true
                )
            ),
            new StringField(
                'user_ext_id',
                array(
                    'required' => true
                )
            ),
            new StringField(
                'user_fio',
                array()
            ),
            new IntegerField(
                'user_id',
                array()
            )
        );
    }

    // костыль
    public static function resetID()
    {
        $iReturn = 0;
        $res = AttendanceTable::getList()->fetchAll();
        if($res) {
            foreach (array_column($res, 'id') as $k => $v) {
                $connection = Application::getConnection();
                $query = 'UPDATE ' . self::getTableName() . ' SET id=' . $k . ' WHERE id=' . $v;
                $result = $connection->query($query);
                $iReturn++;
            }
        }
        return $iReturn;
    }
}
