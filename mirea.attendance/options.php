<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\HttpApplication;

$sModule = 'mirea.attendance';
$sPrefix = 'mirea_attendance';

Loc::loadLanguageFile(__FILE__);

if (!$USER->isAdmin()) {
    $APPLICATION->authForm(Loc::getMessage('MIREA_ATTENDANCE_FORBIDDEN'));
}

Loader::includeModule($sModule);

$arTabs = array(
    array(
        'DIV' => 'GENERAL',
        'TAB' => Loc::getMessage('MIREA_ATTENDANCE_SCUD_SETTINGS'),
        'OPTIONS' => array(
            array(
                $sPrefix . '_api_key',
                Loc::getMessage('MIREA_ATTENDANCE_API_KEY_LABEL'),
                Option::get($sModule, $sPrefix . '_api_key'),
                ['password', 50]
            ),
            array(
                $sPrefix . '_host',
                Loc::getMessage('MIREA_ATTENDANCE_HOST_LABEL'),
                Option::get($sModule, $sPrefix . '_host'),
                ['text', 50],
                'N',
                '*'
            ),
            array(
                $sPrefix . '_port',
                Loc::getMessage('MIREA_ATTENDANCE_PORT_LABEL'),
                Option::get($sModule, $sPrefix . '_host'),
                ['text', 50],
                'N',
                '*'
            ),
            array(
                $sPrefix . '_show_subs',
                Loc::getMessage('MIREA_ATTENDANCE_SHOW_SUBS_LABEL'),
                Option::get($sModule, $sPrefix . '_show_subs'),
                ['checkbox', 'Y']
            ),
            array(
                'note' => Loc::getMessage('MIREA_ATTENDANCE_REQUIRED_NOTE')
            )
        )
    ),
);

$request = HttpApplication::getInstance()->getContext()->getRequest();
if ($request->isPost() and $request['Update'] and check_bitrix_sessid()) {
    foreach ($arTabs as $tab) {
        foreach ($tab["OPTIONS"] as $arOption) {
            if (!is_array($arOption)) {
                continue;
            }
            $optionName = $arOption[0];
            $optionValue = $request->getPost($optionName);

            $arRequired = array(
                $sPrefix . '_host',
                $sPrefix . '_port',
                $sPrefix . '_interval'
            );
            if (in_array($optionName, $arRequired) and !$optionValue) {
                continue;
            }
            try {
                Option::set(
                    $sModule,
                    $optionName,
                    is_array($optionValue) ? implode(",", $optionValue) : $optionValue
                );
            } catch (Exception $e) {
                // Bitrix\Main\Config\Option::set(string, NULL, NULL), но форма сохраняется, хз почему
            }
        }
    }
    header('Location: ' . $_SERVER['REQUEST_URI'] . '#success');
}

$tabControl = new CAdminTabControl('tabControl', $arTabs);
$tabControl->begin(); ?>

<form method="post"
      action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx(
          $request['mid']
      ) ?>&amp;lang=<?= $request["lang"] ?>"
      name="mirea_attendance">
    <?= bitrix_sessid_post(); ?>
    <?
    foreach ($arTabs as $tab) {
        if ($tab["OPTIONS"]) {
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList($sModule, $tab['OPTIONS']);
        }
    }
    $tabControl->Buttons();
    ?>
    <input type="submit" class="adm-btn-save" name="Update" value="<?= Loc::getMessage('MAIN_SAVE') ?>">
    <input type="reset" name="reset" value="<?= Loc::getMessage('MAIN_RESET') ?>">
</form>

<?php
$tabControl->end(); ?>
