<?php

defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Page\Asset;

$sPrefix = 'mirea_attendance';

CJSCore::RegisterExt(
    $sPrefix . '_list',
    array(
        'css' => array(
            '/bitrix/css/main/font-awesome.css',
            '/local/libs/bootstrap/css/bootstrap.min.css',
            $this->GetFolder() . '/css/style.css'
        ),
        'js' => array(
            $this->GetFolder() . '/js/functions.js',
            $this->GetFolder() . '/js/store.js',
            $this->GetFolder() . '/js/ListMain.js',
            $this->GetFolder() . '/js/components/MireaAttendance.js',
            $this->GetFolder() . '/js/components/UserEventsAccordion.js',
        ),
        'rel' => array(
            'ui.vue',
            'ui.vue.vuex'
        ),
        'lang' => array(
            $this->GetFolder() . "/lang/" . LANGUAGE_ID . "/js/componentPhrases.php",
        )
    )
);

?>

<div id="<?= $sPrefix ?>_wrapper">
    <div id="<?= $sPrefix ?>_include">
        <?
        $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => $sPrefix . "_inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => 'include/index.php'
            )
        ); ?>
    </div>

    <div id="<?= $sPrefix ?>_main"></div>
</div>

<script>
    store.state.user = JSON.parse('<?=json_encode($arResult['USER'])?>');
    store.state.subordinates = JSON.parse('<?=json_encode($arResult['SUBORDINATES'])?>');
    store.state.defStart = <?=$arResult['DEFAULT_START'] ?>;
    store.state.defEnd = <?=$arResult['DEFAULT_END'] ?>;
    store.state.curStart = <?=$arResult['DEFAULT_START'] ?>;
    store.state.curEnd = <?=$arResult['DEFAULT_END'] ?>;
</script>

<?
CJSCore::Init(array($sPrefix . '_list'));
?>
