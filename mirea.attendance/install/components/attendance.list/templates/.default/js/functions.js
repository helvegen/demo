function formatEvents(arEvents) {
    let arReturn = [];

    let events = [];
    arEvents.forEach(function (item, key) {
        events.unshift({
            id: Number(item[0]),
            jsUnixTime: Number(item[1]) * 1000,
            type: (key % 2 === 0) ? 'entry' : 'exit'
        });
    })

    let entries = events.filter(event => event.type === 'entry');
    let exits = events.filter(event => event.type === 'exit');

    entries.forEach(function (entry, index) {
        let obEntry = new Date(entry.jsUnixTime);
        let obExit = new Date(exits[index].jsUnixTime);
        let midnight = BX.date.format('Y-m-d', obEntry);

        arReturn[index] = {
            day: BX.date.format('D, d.m.Y', obEntry),
            entry: BX.date.format('H:i', obEntry),
            exit: BX.date.format('H:i', obExit),
            isToday: midnight === BX.date.format('Y-m-d')
        };
    })

    return arReturn;
}
