const store = BX.Vuex.store({
    state: {
        user: {EVENTS: [], EXT_ID: ''},
        subordinates: [],
        defStart: 0,
        defEnd: 0,
        curStart: 0,
        curEnd: 0
    },
    getters: {
        localize() {
            return BX.Vue.getFilteredPhrases('MIREA_ATTENDANCE_');
        },
        GetUser(state) {
            return state.user;
        },
        GetSubs(state) {
            return !!state.subordinates ? state.subordinates : [];
        },
        GetDefaultDates(state) {
            return {
                defStart: BX.date.format('Y-m-d', state.defStart),
                defEnd: BX.date.format('Y-m-d', state.defEnd)
            };
        },
        GetCurrentDates(state) {
            return {
                curStart: BX.date.format('Y-m-d', state.curStart),
                curEnd: BX.date.format('Y-m-d', state.curEnd)
            };
        },
        GetSubID(state, getters) {
            let arReturn = [];
            if (getters.GetSubs.length > 0) {
                state.subordinates.forEach(function (item) {
                    arReturn.push(item.EXT_ID);
                })
            }
            return arReturn;
        }
    },
    actions: {
        async LoadEventsByDate(ctx, params = {startDate: '', endDate: ''}) {
            await BX.ajax.runComponentAction(
                'mirea.attendance:attendance.list',
                'LoadEventsByDate',
                {
                    mode: 'class',
                    data: {params: params}
                }
            ).then(function (response) {
                if (response.status === 'success') {
                    ctx.commit('UpdateUserEvents', response.data);
                }
            });
        }
    },
    mutations: {
        UpdateUserEvents(state, data) {
            let user = state.user;
            let events = user.EVENTS;
            events = data.user;
            user.EVENTS = events;
            state.user = user;

            let subs = !!state.subordinates ? state.subordinates : [];
            if(subs.length > 0) {
                subs.forEach(function(sub, key) {
                    if(!!data.subs[sub.EXT_ID]) {
                        subs[key].EVENTS = data.subs[sub.EXT_ID];
                    }
                })
            }
            state.subordinates = subs;
        },
        UpdateCurrentDate(state, data) {
            let item = state[data.sPath];
            let obDate = new Date(data.sDate);
            item = obDate.getTime() / 1000;
            state[data.sPath] = item;
        }
    }
});