BX.Vue.component(
    'bx-mirea-attendance',
    {
        computed: {
            ...BX.Vuex.mapGetters([
                'localize',
                'GetUser',
                'GetSubs',
                'GetSubID',
                'GetDefaultDates',
                'GetCurrentDates',
            ]),
            startDate: {
                get: function() {
                    return this.GetCurrentDates.curStart;
                },
                set: function(sDate) {
                    this.$store.commit('UpdateCurrentDate', {
                        sDate: !!sDate ? sDate : BX.date.format('Y-m-d'),
                        sPath: 'curStart'
                    });
                }
            },
            endDate: {
                get: function() {
                    return this.GetCurrentDates.curEnd;
                },
                set: function(sDate) {
                    this.$store.commit('UpdateCurrentDate', {
                        sDate: !!sDate ? sDate : BX.date.format('Y-m-d'),
                        sPath: 'curEnd'
                    });
                }
            },
        },
        data() {
            return {}
        },
        mounted() {},
        methods: {
            ...BX.Vuex.mapActions(['LoadEventsByDate']),
            RenewEvents: function () {
                let params = {
                    startDate: this.startDate,
                    endDate: this.endDate,
                    user: this.GetUser.EXT_ID,
                    subs: this.GetSubID
                };
                this.LoadEventsByDate(params);
            },
            ResetEvents: function() {
                this.startDate = this.GetDefaultDates.defStart;
                this.endDate = this.GetDefaultDates.defEnd;
                let params = {
                    startDate: this.startDate,
                    endDate: this.endDate,
                    user: this.GetUser.EXT_ID,
                    subs: this.GetSubID
                };
                this.LoadEventsByDate(params);
            }
        },
        template: `
<div>
    <h5 class="mb-4">{{ localize.MIREA_ATTENDANCE_SHOW }}</h5>
    <form class="form-inline mb-4"
        @submit.prevent="RenewEvents"
        @reset.prevent="ResetEvents">
        <label for="date-start" class="mr-2">{{ localize.MIREA_ATTENDANCE_DATE_START_LABEL }}</label>
        <input type="date"
            class="form-control form-control-sm mr-2 mb-2"
            id="date-start"
            name="date-start"
            v-model="startDate">
        <label for="date-end" class="mr-2">{{ localize.MIREA_ATTENDANCE_DATE_END_LABEL }}</label>
        <input type="date"
            class="form-control form-control-sm mr-2 mb-2"
            id="date-end"
            name="date-end"
            v-model="endDate">
      <button type="submit" class="btn btn-success btn-sm mr-2 mb-2">{{ localize.MIREA_ATTENDANCE_SUBMIT_LABEL }}</button>
      <button type="reset" class="btn btn-danger btn-sm mb-2">{{ localize.MIREA_ATTENDANCE_RESET_LABEL }}</button>
    </form>
    
    <bx-user-events-accordion
        v-bind:userEvents="GetUser.EVENTS"
        v-bind:opened="true"/>
    <template
        v-if="GetSubs.length > 0"
        v-for="sub in GetSubs">
        <bx-user-events-accordion
            v-bind:userID="sub.ID"
            v-bind:userFio="sub.NAME"
            v-bind:userEvents="sub.EVENTS"
            v-bind:opened="false"/>
    </template>
</div>`
    }
);