BX.Vue.component(
    'bx-user-events-accordion',
    {
        computed: {
            ...BX.Vuex.mapGetters(['localize', 'GetSubs']),
            cardTitle: function () {
                return !!this.userFio ? this.userFio : this.localize.MIREA_ATTENDANCE_MY_EVENTS;
            },
        },
        data() {
            return {
                showTable: this.opened
            };
        },
        props: ['userFio', 'userID', 'userEvents', 'opened'],
        methods: {
            ToggleTable: function () {
                this.showTable = !this.showTable;
            }
        },
        template: `
<div class="card border-0 mb-2">
    <div class="card-header font-weight-bolder border">
        <span class="main-grid-cell-content"
            data-prevent-default="true"
            v-if="userID > 0 && !!userFio">
            <a :href="'/company/personal/user/' + userID + '/'">{{ userFio }}</a>
        </span>
        <span v-else>
            {{ cardTitle }}
        </span>
        <button class="btn btn-sm btn-light text-secondary py-0"
            @click="ToggleTable"
            v-if="GetSubs.length > 0">
            <i class="fa fa-caret-up"
                aria-hidden="true"
                v-if="showTable"></i>
            <i class="fa fa-caret-down"
                aria-hidden="true"
                v-else></i>
        </button>
    </div>
    <transition name="slide">
        <table class="table table-hover mb-0 border" v-if="showTable">
        <thead>
            <tr>
                <td>{{ localize.MIREA_ATTENDANCE_WEEKDAY }}</td>
                <td>{{ localize.MIREA_ATTENDANCE_ENTER }}</td>
                <td>{{ localize.MIREA_ATTENDANCE_EXIT }}</td>
            </tr>
        </thead>
        <tbody>
            <tr v-for="event in userEvents">
                <td>{{ event.day }}</td>
                <td>{{ event.entry }}</td>
                <td>
                    {{ event.exit }}
                    <span class="atten-badge"
                        v-if="event.ifToday">
                        <span class="badge font-weight-bold badge-info">?</span>
                        <div class="alert alert-info atten-alert text-center py-1 px-2">{{ localize.MIREA_ATTENDANCE_EXIT_ALERT }}</div>
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
    </transition>
</div>`
    }
);