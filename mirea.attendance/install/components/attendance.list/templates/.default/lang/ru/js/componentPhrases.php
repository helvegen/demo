<?php

$MESS['MIREA_ATTENDANCE_DATE_START_LABEL'] = 'С';
$MESS['MIREA_ATTENDANCE_DATE_END_LABEL'] = 'по';
$MESS['MIREA_ATTENDANCE_SUBMIT_LABEL'] = 'Показать';
$MESS['MIREA_ATTENDANCE_RESET_LABEL'] = 'Сбросить';
$MESS['MIREA_ATTENDANCE_SHOW'] = 'Показать посещаемость';
$MESS['MIREA_ATTENDANCE_MY_EVENTS'] = 'Мои посещения';
$MESS['MIREA_ATTENDANCE_WEEKDAY'] = 'Дата/день недели';
$MESS['MIREA_ATTENDANCE_ENTER'] = 'Время входа';
$MESS['MIREA_ATTENDANCE_EXIT'] = 'Время выхода';
$MESS['MIREA_ATTENDANCE_EXIT_ALERT'] = 'Это последнее зафиксированное время ухода. Окончательные данные будут доступны завтра.';
