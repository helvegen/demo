<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Config\Option;
use Mirea\Attendance\ScudData;

Loc::loadMessages(__FILE__);

class AttendanceListComponent extends \CBitrixComponent implements Controllerable
{
    public function __construct($component = null)
    {
        parent::__construct($component);
        Loader::includeModule('mirea.attendance');
    }

    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadLanguageFile(__FILE__);
    }

    public function executeComponent()
    {
        try {
            $GLOBALS['APPLICATION']->SetTitle(Loc::getMessage('MIREA_ATTENDANCE_PAGE_TITLE'));

            $this->getResult();

            $this->includeComponentTemplate();
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    public function getResult()
    {
        // set default dates to show
        $obMon = new DateTime('+1 day'); // to how only this week, if today is Monday
        $obMon->modify('Last Monday');
        $sStart = $obMon->format('d.m.Y') . ' 00:00:00';
        $sEnd = date('d.m.Y 23:59:59');

        $this->arResult['DEFAULT_START'] = strtotime($sStart);
        $this->arResult['DEFAULT_END'] = strtotime($sEnd);

        global $USER;
        // get user`s data
        $arUser = CUser::GetByID($USER->GetID())->GetNext();
        $this->arResult['USER'] = array(
            'ID' => $arUser['ID'],
            'NAME' => $USER->GetFullName(),
            'EXT_ID' => $arUser['XML_ID'],
            'LOGIN' => $USER->GetLogin()
        );
        $arData = ScudData::GetUserData(
            array(
                'EXT_ID' => $arUser['XML_ID'],
                'START' => $sStart,
                'END' => $sEnd
            )
        )['EVENTS'];
        $this->arResult['USER']['EVENTS'] = $this->formatEvents($arData);

        // get users`s slaves
        if(Option::get('mirea.attendance', 'mirea_attendance_show_subs') == 'Y') {
            $arDepts = $arUser['UF_DEPARTMENT'];
            foreach ($arDepts as $k => $iDept) {
                if ($arUser['ID'] != CIntranetUtils::GetDepartmentManagerID($iDept)) {
                    unset($arDepts[$k]);
                }
            }
            $res = CIntranetUtils::getDepartmentEmployees($arDepts);
            while ($ar = $res->GetNext()) {
                if ($ar['ID'] != $arUser['ID']) {
                    $arSub = array(
                        'ID' => $ar['ID'],
                        'NAME' => implode(' ', array($ar['LAST_NAME'], $ar['NAME'], $ar['SECOND_NAME'])),
                        'EXT_ID' => $ar['XML_ID'],
                        'LOGIN' => $ar['LOGIN'],
                        'IS_ONLINE' => CUser::IsOnLine($ar['ID'])
                    );
                    $arData = ScudData::GetUserData(
                        array(
                            'EXT_ID' => $ar['XML_ID'],
                            'START' => $sStart,
                            'END' => $sEnd
                        )
                    )['EVENTS'];
                    $arSub['EVENTS'] = $this->formatEvents($arData);
                    $this->arResult['SUBORDINATES'][] = $arSub;
                }
            }
        }
    }

    public function formatEvents($arUnixEvents)
    {
        $arReturn = array();

        $arEntries = array_keys(
            array_filter(
                array_flip(array_values($arUnixEvents)),
                function ($item) {
                    return ($item % 2 !== 0);
                }
            )
        );
        $arExits = array_keys(
            array_filter(
                array_flip(array_values($arUnixEvents)),
                function ($item) {
                    return ($item % 2 === 0);
                }
            )
        );
        $iMidnight = strtotime(date('Y-m-d'));

        foreach ($arEntries as $k => $v) {
            $arReturn[] = array(
                'day' => FormatDate('D, d.m.Y', $v),
                'entry' => FormatDate('H:i', $v),
                'exit' => FormatDate('H:i', $arExits[$k]),
                'ifToday' => $iMidnight == strtotime(date('Y-m-d', $v))
            );
        }

        return $arReturn;
    }

    public function configureActions()
    {
        return array(
            'LoadEventsByDate' => array(
                'prefilters' => [
                    new ActionFilter\Authentication
                ],
            )
        );
    }

    public function LoadEventsByDateAction($params)
    {
        $arReturn = array();

        $ar = ScudData::GetUserData(
            array(
                'EXT_ID' => $params['user'],
                'START' => $params['startDate'] ?
                    date('d.m.Y 00:00:00', strtotime($params['startDate'])) :
                    date('d.m.Y 00:00:00'),
                'END' => $params['endDate'] ?
                    date('d.m.Y 23:59:59', strtotime($params['endDate'])) :
                    date('d.m.Y 23:59:59'),
            )
        )['EVENTS'];
        $arReturn['user'] = $this->formatEvents($ar);

        if($params['subs'] and is_array($params['subs'])) {
            foreach ($params['subs'] as $v) {
                $ar = ScudData::GetUserData(
                    array(
                        'EXT_ID' => $v,
                        'START' => $params['startDate'] ?
                            date('d.m.Y 00:00:00', strtotime($params['startDate'])) :
                            date('d.m.Y 00:00:00'),
                        'END' => $params['endDate'] ?
                            date('d.m.Y 23:59:59', strtotime($params['endDate'])) :
                            date('d.m.Y 23:59:59'),
                    )
                )['EVENTS'];
                $arReturn['subs'][$v] = $this->formatEvents($ar);
            }
        }

        return $arReturn;
    }

}
