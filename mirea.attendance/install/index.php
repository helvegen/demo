<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

class mirea_attendance extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        $this->MODULE_ID = 'mirea.attendance';
        $this->MODULE_PREFIX = 'mirea_attendance';
        $this->MODULE_NAME = Loc::getMessage('MIREA_ATTENDANCE_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MIREA_ATTENDANCE_MODULE_DESCRIPTION');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = Loc::getMessage('MIREA_ATTENDANCE_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installDB();
        $this->installFiles();
        $this->installAgents();
    }

    public function doUninstall()
    {
        $this->unInstallAgents();
        $this->uninstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function installDB()
    {
        Loader::includeModule($this->MODULE_ID);

        $arEntities = array(
            'Mirea\Attendance\AttendanceTable'
        );
        $connection = Application::getConnection();
        foreach ($arEntities as $sEntity) {
            $tableName = $sEntity::getTableName();
            $arFields = $sEntity::getMap();
            if (!$connection->isTableExists($tableName)) {
                $connection->createTable($tableName, $arFields);
            }
        }
    }

    public function installFiles()
    {
        $arDirs = array(
            'attendance' => $_SERVER['DOCUMENT_ROOT'] . '/attendance',
            'components' => $_SERVER['DOCUMENT_ROOT'] . '/local/components/' . $this->MODULE_ID
        );
        foreach ($arDirs as $dir => $destination) {
            $from = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/' . $dir;
            if (Directory::isDirectoryExists($from) and !Directory::isDirectoryExists($destination)) {
                CopyDirFiles($from, $destination, false, true);
            }
        }
    }

    public function installAgents()
    {
        $sDate = date('d.m.Y H:i:s', strtotime('+1 hour')); // to check agent in 1 hour, not now
        CAgent::AddAgent(
            '\Mirea\Attendance\ScudData::AddOrUpdateAgent();',
            $this->MODULE_ID,
            'N',
            60*60*2, // 2 hours
            $sDate,
            'Y',
            $sDate
        );

        $sDate = date('d.m.Y H:i:s', strtotime('+1 week')); // to check agent in 1 week, not now
        CAgent::AddAgent(
            '\Mirea\Attendance\ScudData::DeleteOldDataAgent();',
            $this->MODULE_ID,
            'N',
            60*60*24*7, // 1 week
            $sDate,
            'Y',
            $sDate
        );
    }

    public function unInstallAgents()
    {
        CAgent::RemoveModuleAgents($this->MODULE_ID);
    }
}
