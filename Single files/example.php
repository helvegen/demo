<?php

namespace Findchia\Service\WSRequestHandler\FAQ;

use Amp\Promise;
use Findchia\Core\Websocket\ContextedMessage;
use Findchia\Core\Websocket\RequestHandler\BaseRequestHandler;
use Findchia\Model\FAQ\FAQ;
use Findchia\Model\FAQ\Section;
use Findchia\Model\Localization\Phrase;
use Proto\Findchia\Message\Faq\FaqSubtopic;
use Proto\Findchia\Message\Faq\FaqTopicMessage;
use Proto\Findchia\Message\Faq\QA;
use function Amp\call;

class GetFaqSectionContentHandler extends BaseRequestHandler
{
    public function handleRequest(?ContextedMessage $message): Promise
    {
        return call(function () use ($message) {
            /** @var FaqTopicMessage $dataMessage */
            $dataMessage = $message->dataMessage;
            $langCode = Phrase::recognizeLangCode($dataMessage->getLangCode());

            /** @var Section $parentSection */
            $parentSection = yield Section::query()
                ->where(['code' => $dataMessage->getCode()])
                ->one();

            if (!$parentSection) {
                throw new \LogicException('Wrong section code');
            }

            // start building response
            $topicMessage = new FaqTopicMessage();
            $topicMessage->setLangCode($langCode);
            $topicMessage->setCode($dataMessage->getCode());

            // get raw subsections
            $subsections = yield Section::query()
                ->where(['parent_section_id' => $parentSection->sectionId])
                ->all();

            if ($subsections) {
                // get raw qas
                $qas = yield FAQ::query()
                    ->whereIn('section_id', array_column($subsections, 'sectionId'))
                    ->all();

                if ($qas) {
                    // get full langpack
                    $phraseIds = array_unique(
                        array_merge(
                            array_column($subsections, 'titleId'),
                            array_column($qas, 'questionId'),
                            array_column($qas, 'answerId')
                        )
                    );

                    $langPack = yield Phrase::query()
                        ->whereIn('phrase_id', $phraseIds)
                        ->all();
                    $langColumn = yield Phrase::formatLangCode($langCode);

                    // build subsections data
                    $subsectionsBuilt = [];
                    foreach ($subsections as $subsection) {
                        /** @var Section $subsection */
                        $subsectionsBuilt[] = (new FaqSubtopic())
                            ->setId($subsection->getId())
                            ->setTitle($langPack[$subsection->titleId]->$langColumn)
                            ->setQa(array_filter(
                                array_map(function (FAQ $qa) use ($subsection, $langPack, $langColumn) {
                                    if ($qa->sectionId === $subsection->sectionId) {
                                        return (new QA())
                                            ->setId($qa->faqId)
                                            ->setQuestion($langPack[$qa->questionId]->$langColumn)
                                            ->setAnswer($langPack[$qa->answerId]->$langColumn);
                                    }
                                    return false;
                                }, $qas)
                            ))
                        ;
                    }

                    $topicMessage->setSubtopic($subsectionsBuilt);
                }

            } else {
                $topicMessage->setSubtopic([]);
            }

            return $topicMessage;
        });
    }
}