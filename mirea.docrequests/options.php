<?php

use Bitrix\Main\HttpApplication;
use Mirea\DocRequests\Config;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use CGroup;

Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
Loc::loadLanguageFile(__FILE__);

if (!$USER->isAdmin()) {
    $APPLICATION->authForm(Loc::getMessage('MIREA_DOCREQUESTS_FORBIDDEN'));
}

$obConfig = new Config();
$obConfig->CheckModule();
$obConfig->CheckReferences();

// инфоблоки модуля
$arBlocks = array(
    0 => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED')
);
foreach ($obConfig->moduleIBlocks as $iblock) {
    $arBlocks[$iblock['ID']] = $iblock['NAME'];
}
// инфоблоки модуля - конец

// юзеры
$arUsers = array(
    0 => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED')
);
$res = CUser::GetList(
    $by = 'last_name',
    $order = 'asc',
    array('ACTIVE' => 'Y')
);
while ($ar = $res->GetNext()) {
    $arName = array(
        trim($ar['LAST_NAME']),
        trim($ar['NAME']),
        trim($ar['SECOND_NAME'])
    );
    if ($ar['WORK_POSITION']) {
        $arName[] = "({$ar['WORK_POSITION']})";
    }

    if($name = implode(' ', array_filter($arName))) {
        $arUsers[$ar['ID']] = $name;
    }
}
// юзеры - конец

// группы юзеров
$arGroups = array(
    0 => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED')
);
$res = CGroup::GetList(
    $by = 'id',
    $order = 'desc',
    array('ACTIVE' => 'Y')
);
while($ar = $res->GetNext()) {
    $arGroups[$ar['ID']] = "{$ar['NAME']} [{$ar['ID']}]";
}
// группы юзеров - конец

// способы оповещения
$arCalls = array(
    0 => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED'),
    'email' => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_EMAIL'),
    'push' => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_PUSH'),
    'no' => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_NO_CALL')
);
// способы оповещения - конец

$arTabs = array(
    array(
        'DIV' => 'SUBSTITUTION',
        'TAB' => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_SETTINGS'),
        'OPTIONS' => array(
            array(
                $obConfig->MODULE_PREFIX . '_requests_iblock',
                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_REQUESTS_IBLOCK'),
                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_requests_iblock']['value'],
                array('selectbox', $arBlocks)
            ),
            array(
                $obConfig->MODULE_PREFIX . '_lists_iblock',
                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_LISTS_IBLOCK'),
                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_lists_iblock']['value'],
                array('selectbox', $arBlocks)
            ),
//            array(
//                $obConfig->MODULE_PREFIX . '_resp_0_call',
//                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESP_0_CALL'),
//                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_0_call']['value'],
//                array('selectbox', $arCalls)
//            ),
            array(
                $obConfig->MODULE_PREFIX . '_resp_1_user',
                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESP_1_USER'),
                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_1_user']['value'],
                array('selectbox', $arGroups)
            ),
//            array(
//                $obConfig->MODULE_PREFIX . '_resp_1_call',
//                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESP_1_CALL'),
//                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_1_call']['value'],
//                array('selectbox', $arCalls)
//            ),
            array(
                $obConfig->MODULE_PREFIX . '_resp_2_user',
                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESP_2_USER'),
                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_2_user']['value'],
                array('selectbox', $arUsers)
            ),
//            array(
//                $obConfig->MODULE_PREFIX . '_resp_2_call',
//                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESP_2_CALL'),
//                $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_2_call']['value'],
//                array('selectbox', $arCalls)
//            ),
            array(
                $obConfig->MODULE_PREFIX . '_institute_marker',
                Loc::getMessage('MIREA_DOCREQUESTS_INSTITUTION_MARKER'),
                Option::get($obConfig->MODULE_ID, $obConfig->MODULE_PREFIX . '_institute_marker'),
                array('textarea', 3, 110),
                'N',
                '<span style="color: black">1</span>'
            ),
            array(
                'note' => Loc::getMessage('MIREA_DOCREQUESTS_INSTITUTION_MARKER_NOTE')
            )
        )
    )
);

$request = HttpApplication::getInstance()->getContext()->getRequest();
if ($request->isPost() and $request['Update'] and check_bitrix_sessid()) {
    foreach ($arTabs as $tab) {
        foreach ($tab["OPTIONS"] as $arOption) {
            if (!is_array($arOption)) {
                continue;
            }
            $optionName = $arOption[0];
            $optionValue = $request->getPost($optionName);
            try {
                Option::set(
                    $obConfig->MODULE_ID,
                    $optionName,
                    is_array($optionValue) ? implode(",", $optionValue) : $optionValue
                );
            } catch (Exception $e) {
                $obConfig->lastError = $e;
            }
        }
    }
    header('Location: ' . $_SERVER['REQUEST_URI'] . '#success');
}

$tabControl = new CAdminTabControl('tabControl', $arTabs);
$tabControl->begin(); ?>

<form method="post"
      action="<?= $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx(
          $request['mid']
      ) ?>&amp;lang=<?= $request["lang"] ?>"
      name="<?= $obConfig->MODULE_PREFIX ?>">
    <?= bitrix_sessid_post(); ?>
    <?

    foreach ($arTabs as $tab) {
        if ($tab["OPTIONS"]) {
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList($obConfig->MODULE_ID, $tab['OPTIONS']);
        }
    }
    $tabControl->Buttons();
    ?>
    <input type="submit" class="adm-btn-save" name="Update" value="<?= Loc::getMessage('MAIN_SAVE') ?>">
    <input type="reset" name="reset" value="<?= Loc::getMessage('MAIN_RESET') ?>">
</form>

<?php

$tabControl->end(); ?>
