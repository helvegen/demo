<?php

namespace Mirea\DocRequests;

use Bitrix\Main\Localization\Loc;
use CIBlock;
use CIBlockType;
use CIBlockProperty;
use CGroup;

Loc::loadLanguageFile(__FILE__);

class Importer extends Config
{
    public $INSTALL_USERGROUPS_PATH;
    public $INSTALL_IBLOCK_PATH;
    public $INSTALL_IBLOCK_PROPERTIES_PATH;

    public function __construct()
    {
        $this->INSTALL_USERGROUPS_PATH = $_SERVER['DOCUMENT_ROOT'] . $this->MODULE_ROOT . '/install/usergroups';
        $this->INSTALL_IBLOCK_PATH = $_SERVER['DOCUMENT_ROOT'] . $this->MODULE_ROOT . '/install/iblocks';
        $this->INSTALL_IBLOCK_PROPERTIES_PATH = $_SERVER['DOCUMENT_ROOT'] . $this->MODULE_ROOT . '/install/iblockproperties';

        parent::__construct();
    }

    public function InstallIBlockTypes()
    {
        $arType = CIBlockType::GetByID($this->MODULE_LIST_TYPE)->Fetch();
        if (!$arType) {
            $arFields = array(
                'ID' => $this->MODULE_LIST_TYPE,
                'SECTIONS' => 'N',
                'IN_RSS' => 'N',
                'SORT' => 100,
                'LANG' => array(
                    'ru' => array(
                        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_MODULE_LIST_TYPE_NAME'),
                        'SECTION_NAME' => Loc::getMessage('MIREA_DOCREQUESTS_SECTIONS_NAME'),
                        'ELEMENT_NAME' => Loc::getMessage('MIREA_DOCREQUESTS_ELEMENTS_NAME')
                    )
                )
            );
            $ob = new CIBlockType;
            $res = $ob->Add($arFields);
        }
        if ($res or $arType) {
            $iblocks = $this->InstallIBlocks();
            if ($iblocks) {
                $this->InstallIBlockProperties($iblocks);
            };
        };
    }

    public function InstallUserGroups()
    {
        $arReturn = array();
        $arGroups = array_values(
            array_diff(
                scandir($this->INSTALL_USERGROUPS_PATH),
                array('.', '..')
            )
        );
        foreach ($arGroups as $k => $group) {
            $groupPath = $this->INSTALL_USERGROUPS_PATH . '/' . $group;
            if (is_file($groupPath)) {
                include $groupPath;

                if ($arGroupFields and is_array($arGroupFields)) {
                    $search = CGroup::GetList(
                        $by = 'id',
                        $order = 'asc',
                        array(
                            'STRING_ID' => pathinfo($groupPath)['filename']
                        )
                    );
                    if ($search->AffectedRowsCount() === 0) {
                        $arGroupFields['ACTIVE'] = 'Y';
                        $arGroupFields['C_SORT'] = ++$k * 10;
                        $arGroupFields['STRING_ID'] = pathinfo($groupPath)['filename'];
                        $ob = new CGroup();
                        $res = $ob->Add($arGroupFields);
                        if($res) {
                            $arReturn[$arGroupFields['STRING_ID']] = $res;
                        } else {
                            $this->lastError = $ob->LAST_ERROR;
                        }
                    }
                }
            }
        }

        return $arReturn;
    }

    protected function InstallIBlockProperties($iblocks)
    {
        $arBlocks = array_values(
            array_diff(
                scandir($this->INSTALL_IBLOCK_PROPERTIES_PATH),
                array('.', '..')
            )
        );
        foreach ($arBlocks as $k => $block) {
            $propPath = $this->INSTALL_IBLOCK_PROPERTIES_PATH . '/' . $block;
            if (is_file($propPath)) {
                include $propPath;

                if ($arIBlockProps and is_array($arIBlockProps)) {
                    foreach ($arIBlockProps as $kk => $arPropFields) {
                        $arPropFields['IBLOCK_ID'] = $iblocks[pathinfo($propPath)['filename']];
                        $arPropFields['ACTIVE'] = 'Y';
                        $arPropFields['SORT'] = ++$kk * 10;

                        $obProp = new CIBlockProperty();
                        $obProp->Add($arPropFields);
                    }
                }
            }
        }
    }

    protected function InstallIBlocks()
    {
        $arReturn = array();
        $typePath = $this->INSTALL_IBLOCK_PATH;
        $arBlocks = array_values(
            array_diff(
                scandir($typePath),
                array('.', '..')
            )
        );
        foreach ($arBlocks as $k => $block) {
            $iblockPath = $typePath . '/' . $block;
            if (is_file($iblockPath)) {
                include $iblockPath;
                if ($arIBlockFields and is_array($arIBlockFields)) {
                    $res = CIBlock::GetList(
                        array('ID' => 'ASC'),
                        array('CODE' => pathinfo($iblockPath)['filename'])
                    );
                    if($res->AffectedRowsCount() == 0) {
                        $arIBlockFields['CODE'] = pathinfo($iblockPath)['filename'];
                        $arIBlockFields['IBLOCK_TYPE_ID'] = $this->MODULE_LIST_TYPE;
                        $arIBlockFields['LID'] = 's1';
                        $arIBlockFields['ACTIVE'] = 'Y';
                        $arIBlockFields['SORT'] = ++$k * 10;
                        $arIBlockFields['VERSION'] = 2; // хранение свойств в отдельной таблице
                        $ob = new CIBlock();
                        $res = $ob->Add($arIBlockFields);
                        if ($res) {
                            $arReturn[$arIBlockFields['CODE']] = $res;
                        } else {
                            $this->lastError = $ob->LAST_ERROR;
                        }
                    }
                }
            }
        }
        return $arReturn;
    }
}
