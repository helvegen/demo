<?php

namespace Mirea\DocRequests;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use CIBlock;
use CIBlockProperty;
use CUser;

Loc::loadLanguageFile(__FILE__);

class Config
{
    public $MODULE_LIST_TYPE = 'mirea_docrequests';
    public $MODULE_ID = 'mirea.docrequests';
    public $MODULE_ROOT = '/local/modules/mirea.docrequests';
    public $MODULE_PREFIX = 'mirea_docrequests';
    public $MODULE_GROUP_RIGHTS = 'N';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public $moduleOptions;
    public $lastError;
    public $moduleIBlocks;
    public $moduleProps;
    public $moduleEnums;

    public function __construct()
    {
        $this->MODULE_NAME = Loc::getMessage('MIREA_DOCREQUESTS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('MIREA_DOCREQUESTS_MODULE_DESCRIPTION');

        $arModuleVersion = [];
        include($_SERVER['DOCUMENT_ROOT'] . $this->MODULE_ROOT . '/install/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->PARTNER_NAME = Loc::getMessage('MIREA_DOCREQUESTS_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('MIREA_DOCREQUESTS_PARTNER_URI');

        $this->getModuleOptions();
        $this->getModuleIBlocks();
        $this->getModuleProps();
        $this->getModuleEnums();
    }

    public function getModuleOptions()
    {
        try {
            $arOpt = Option::getForModule($this->MODULE_ID);
            foreach ($arOpt as $optionName => $optionValue) {
                $this->moduleOptions[$optionName] = [
                    'id' => $optionName,
                    'name' => Loc::getMessage(ToUpper($optionName)),
                    'value' => $arOpt[$optionName]
                ];
            }
        } catch (\Exception $e) {
            $this->lastError = $e;
            $GLOBALS['APPLICATION']->ThrowException(Loc::getMessage("MIREA_DOCREQUESTS_OPTIONS_ERROR"));
        }
    }

    public function CheckReferences()
    {
        try {
            Loader::includeModule('iblock');
            Loader::includeModule('im');
            return true;
        } catch (\Exception $e) {
            $this->lastError = $e;
            $GLOBALS['APPLICATION']->ThrowException(Loc::getMessage("MIREA_DOCREQUESTS_REFERENCE_ERROR"));
            return false;
        }
    }

    public function CheckModule()
    {
        try {
            Loader::includeModule($this->MODULE_ID);
            return true;
        } catch (\Exception $e) {
            $this->lastError = $e;
            $GLOBALS['APPLICATION']->ThrowException(Loc::getMessage("MIREA_DOCREQUESTS_MODULE_ERROR"));
            return false;
        }
    }

    protected function getModuleIBlocks()
    {
        $res = CIBlock::GetList(
            array('ID' => 'ASC'),
            array(
                'TYPE' => $this->MODULE_LIST_TYPE,
            )
        );
        while ($ar = $res->GetNext()) {
            $this->moduleIBlocks[$ar['CODE']] = array(
                'ID' => $ar['ID'],
                'CODE' => $ar['CODE'],
                'NAME' => $ar['NAME'],
                'ACTIVE' => $ar['ACTIVE']
            );
        }
    }

    protected function getModuleProps()
    {
        if($this->moduleIBlocks) {
            foreach ($this->moduleIBlocks as $ar) {
                $rsProps = CIBlockProperty::GetList(
                    array('id' => 'asc'),
                    array(
                        'IBLOCK_ID' => $ar['ID'],
                    )
                );
                while($arProps = $rsProps->GetNext()) {
                    $this->moduleProps[$arProps['CODE']] = array(
                        'ID' => $arProps['ID'],
                        'IBLOCK_ID' => $arProps['IBLOCK_ID'],
                        'CODE' => $arProps['CODE'],
                        'NAME' => $arProps['NAME'],
                        'ACTIVE' => $arProps['ACTIVE'],
                        'PROPERTY_TYPE' => $arProps['PROPERTY_TYPE'],
                        'LIST_TYPE' => $arProps['LIST_TYPE'],
                        'XML_ID' => $arProps['XML_ID'],
                        'MULTIPLE' => $arProps['MULTIPLE']
                    );
                }
            }
        }
    }

    protected function getModuleEnums()
    {
        if($this->moduleProps) {
            $arLists = array_filter(
                $this->moduleProps,
                function($ar) {return $ar['PROPERTY_TYPE']=='L';}
            );
            foreach ($arLists as $arProp) {
                $rsEnums = CIBlockProperty::GetPropertyEnum(
                    $arProp['ID'],
                    array('id' => 'asc')
                );

                while($arEnum = $rsEnums->GetNext()) {
                    $this->moduleEnums[$arEnum['XML_ID']] = array(
                        'ID' => $arEnum['ID'],
                        'PROPERTY_ID' => $arEnum['PROPERTY_ID'],
                        'XML_ID' => $arEnum['XML_ID'],
                        'VALUE' => $arEnum['VALUE']
                    );
                }
            }
        }
    }

    public static function GetUserName($user=0)
    {
        if(!$user) {$user = $GLOBALS['USER']->GetID();}
        $arUser = CUser::GetByID($user)->Fetch();
        $arName = array_filter(
            array(
                $arUser['LAST_NAME'],
                $arUser['NAME'],
//                $arUser['SECOND_NAME']
            )
        );
        return implode(' ', $arName);
    }
}
