<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Подача заявки",
    "DESCRIPTION" => 'Подача заявки на замещение вакантной должности',
    "ICON" => "",
    "CACHE_PATH" => "Y",
    "COMPLEX" => "N"
);
