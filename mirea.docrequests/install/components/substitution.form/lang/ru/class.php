<?php

$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_FORM_PAGE_TITLE'] = 'Подача заявки на замещение вакантной должности';
$MESS['MIREA_DOCREQUESTS_NO_MODULE_OPTIONS'] = 'Отсутствуют необходимые настройки модуля.';
$MESS['MIREA_DOCREQUESTS_CONTACT_ADMIN'] = 'Обратитесь к администратору портала.';
$MESS['MIREA_DOCREQUESTS_ADD_REQUEST_OK'] = 'Ваша заявка успешно создана и отправлена на согласование';
$MESS['MIREA_DOCREQUESTS_APPROVE_REQUEST_OK'] = 'Вы согласовали заявку';
$MESS['MIREA_DOCREQUESTS_CANCEL_REQUEST_OK'] = 'Вы отклонили заявку';
$MESS['MIREA_DOCREQUESTS_REQUEST_CANCELLED'] = 'Ваша заявка отклонена.';
$MESS['MIREA_DOCREQUESTS_REQUEST_USER_APPROVED'] = 'Заявка согласована пользователем';
$MESS['MIREA_DOCREQUESTS_REQUEST_SHOW'] = 'Просмотр';
$MESS['MIREA_DOCREQUESTS_REQUEST_NEED_APPROVE'] = 'Прошу согласовать заявку';
$MESS['MIREA_DOCREQUESTS_REQUEST_NEED_APPROVE'] = 'Прошу согласовать заявку';