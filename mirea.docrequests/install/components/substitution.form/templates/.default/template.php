<?php

defined('B_PROLOG_INCLUDED') || die;

$sPrefix = "{$arResult['MODULE_CONFIG']['MODULE_PREFIX']}_substitution";
$bootstrapPath = $arResult['MODULE_CONFIG']['MODULE_ROOT'] . '/references/bootstrap-5.0.0-beta1';

CJSCore::RegisterExt(
    "{$sPrefix}_form",
    array(
        'css' => array(
            '/local/libs/bootstrap/css/bootstrap.min.css', // костыль для отступов
            '/bitrix/css/main/font-awesome.css',
            $this->GetFolder() . '/css/style.css',
            $bootstrapPath . '/css/bootstrap.min.css',
        ),
        'js' => array(
            $this->GetFolder() . '/js/store.js',
            $this->GetFolder() . '/js/FormMain.js',
            $this->GetFolder() . '/js/components/SubsForm.js',
            $this->GetFolder() . '/js/components/SubsRequest.js',
            $this->GetFolder() . '/js/components/SubsList.js',
        ),
        'rel' => array(
            'ui.vue',
            'ui.vue.vuex',
            'ui.vue.router'
        ),
        'lang' => array(
            $this->GetFolder() . "/lang/" . LANGUAGE_ID . "/js/componentPhrases.php",
        )
    )
);

//echo '<pre>'; print_r($arResult); echo '</pre>';

?>

<form action="" method="post">
    <?= bitrix_sessid_post(); ?>
    <div id="<?= $sPrefix ?>_form_main"></div>
</form>

<script>
    store.state.institutes = <?=CUtil::PhpToJSObject($arResult['INSTITUTES'])?>;
    <? if($arResult['REQUEST']): ?>
        store.state.current = <?=CUtil::PhpToJSObject(
            array(
                'id' => $arResult['REQUEST']['ID'] ? $arResult['REQUEST']['ID'] : 0,
                'institute' => $arResult['REQUEST']['INSTITUTE'] ? $arResult['REQUEST']['INSTITUTE'] : 0,
                'department' => $arResult['REQUEST']['DEPARTMENT'] ? $arResult['REQUEST']['DEPARTMENT'] : 0,
                'position' => $arResult['REQUEST']['NAME'] ? $arResult['REQUEST']['NAME'] : '',
                'term' => $arResult['REQUEST']['REQUEST_TIME'] ? $arResult['REQUEST']['REQUEST_TIME'] : '',
                'work_part' => $arResult['REQUEST']['WORK_PART'] ? $arResult['REQUEST']['WORK_PART'] : 0.1,
            )
        )?>;
        store.state.canEdit = <?=CUtil::PhpToJSObject($arResult['CAN_EDIT']) ?>;
        store.state.canApprove = <?=CUtil::PhpToJSObject($arResult['CAN_APPROVE']) ?>
    <? endif; ?>

    <? if($arResult['LIST']): ?>
        store.state.table = <?=CUtil::PhpToJSObject($arResult['LIST'])?>;
        store.state.table.fio = '<?=$arResult['LIST']['NAME']?>';
        store.state.current.list = '<?=$arResult['LIST']['ID']?>';
    <? endif; ?>
</script>

<?

CJSCore::Init(array("{$sPrefix}_form"));
?>
