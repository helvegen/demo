const store = BX.Vuex.store({
    state: {
        canEdit: true,
        canApprove: false,
        tabList: ['request', 'list'],
        institutes: [],
        departments: [],
        current: {
            id: 0,
            institute: 0,
            department: 0,
            position: '',
            term: '',
            work_part: 0.1,
            list: 0
        },
        table: {
            fio: '',
            curPosition: '',
            cur_work_part: '',
            last: '',
            work_order: '',
            birthday: '',
            base_edu: '',
            science_level: '',
            sec_edu: '',
            quolif_up: '',
            edu_disciplins: '',
            pub_num: '',
            science_articles_rus: '',
            science_articles_vak: '',
            science_articles_scopus: '',
            science_articles_tht: '',
            science_articles_rtzh: '',
            intl_speech: '',
            patent_num: '',
            dh_num: '',
            membership_council: '',
            membership_rinc: 'N',
            membership_expert: 'N',
            membership_tht_rtzh: 'N',
            work_invite: '',
            edu_course: '',
            reward: 'N',
            exp: '',
        },
        tableRequired: [
            'fio',
            'curPosition',
            'cur_work_part',
            'work_order',
            'birthday',
            'base_edu',
            'science_level',
            'sec_edu',
            'quolif_up',
            'edu_disciplins',
            'pub_num',
            'science_articles_rus',
            'science_articles_vak',
            'science_articles_scopus',
            'science_articles_tht',
            'science_articles_rtzh',
            'intl_speech',
            'patent_num',
            'dh_num',
            'membership_council',
            'membership_rinc',
            'membership_expert',
            'membership_tht_rtzh',
            'work_invite',
            'edu_course',
            'reward',
            'exp',
        ],
        formResult: {
            result: '',
            message: '',
            resID: 0
        }
    },
    getters: {
        localize() {return BX.Vue.getFilteredPhrases('MIREA_DOCREQUESTS_SUBSTITUTION_');},
        getTabList(state) {return state.tabList;},
        getInstitutes(state) {return state.institutes;},
        getDepartments(state) {return state.departments;},
        getCurrentData(state) {return state.current;},
        getTableData(state) {return state.table;},
        getCanSubmit(state) {
            let res = true;
            state.tableRequired.forEach(function(item) {
                if(!state.table[item]) {
                    res = false;
                }
            });
            return res;
        },
        getFormResult(state) {return state.formResult;},
        getCanEdit(state) {return state.canEdit;},
        getCanApprove(state) {return state.canApprove;}
    },
    actions: {
        async loadInstituteDepartments(ctx, data=0) {
            await BX.ajax.runComponentAction(
                'mirea.docrequests:substitution.form',
                'loadInstituteDepartments',
                {
                    mode: 'class',
                    data: {data}
                }
            ).then(function(response) {
                if (response.status === 'success') {
                    ctx.commit('updateDepartments', response.data);
                }
            });
        },
        async loadFormResult(ctx, data={}) {
            await BX.ajax.runComponentAction(
                'mirea.docrequests:substitution.form',
                'saveForm',
                {
                    mode: 'class',
                    data: {data}
                }
            ).then(function(response) {
                let res = {
                    result: response.data.TYPE,
                    resID: response.data.ID
                };
                if(response.status === 'success') {
                    res.message = response.data.MESSAGE;
                } else {
                    res.message = getters.localize.MIREA_DOCREQUESTS_SUBSTITUTION_UNKNOWN_ERROR;
                }
                ctx.commit('updateFormResult', res);
            });
        },
        async loadFormAction(ctx, data={}) {
            await BX.ajax.runComponentAction(
                'mirea.docrequests:substitution.form',
                'changeStatus',
                {
                    mode: 'class',
                    data: {data}
                }
            ).then(function(response) {
                let res = {
                    result: response.data.TYPE,
                    resID: response.data.ID
                };
                if(response.status === 'success') {
                    res.message = response.data.MESSAGE;
                } else {
                    res.message = getters.localize.MIREA_DOCREQUESTS_SUBSTITUTION_UNKNOWN_ERROR;
                }
                ctx.commit('updateFormResult', res);
            });
        }
    },
    mutations: {
        updateCurrent(state, data) {
            let ob = state.current;
            if(!!data.target) {
                ob[data.target] = data.value ? data.value : 0;
            }
            state.current = ob;
        },
        updateTable(state, data) {
            let ob = state.table;
            if(!!data.target) {
                ob[data.target] = data.value ? data.value : 0;
            }
            state.table = ob;
        },
        updateDepartments(state, data) {
            let ar = state.departments;
            ar = data;
            state.departments = ar;
        },
        updateFormResult(state, data) {
            let ob = state.formResult;
            ob = data;
            state.formResult = ob;
        }
    }
});