BX.Vue.component(
    'bx-docrequests-subs-request',
    {
        computed: {
            ...BX.Vuex.mapGetters(['localize', 'getInstitutes', 'getCurrentData', 'getDepartments', 'getCanEdit']),
            curInstitute: {
                get: function () {
                    return Number(this.getCurrentData.institute);
                },
                set: function (data) {
                    this.curDepartment = 0;
                    this.$store.commit('updateCurrent', {
                        value: Number(data),
                        target: 'institute'
                    });
                }
            },
            curDepartment: {
                get: function () {
                    return Number(this.getCurrentData.department);
                },
                set: function (data) {
                    this.$store.commit('updateCurrent', {
                        value: Number(data),
                        target: 'department'
                    });
                }
            },
            disabledText: function () {
                let ar = [];
                if(this.curInstitute == 0) {
                    ar.push(this.localize.MIREA_DOCREQUESTS_SUBSTITUTION_INSTITUTE_TITLE.toLowerCase());
                }
                if(this.curDepartment == 0) {
                    ar.push(this.localize.MIREA_DOCREQUESTS_SUBSTITUTION_DEPARTMENT.toLowerCase());
                }
                return this.localize.MIREA_DOCREQUESTS_SUBSTITUTION_SELECT + ' ' + ar.join(this.localize.MIREA_DOCREQUESTS_SUBSTITUTION_GLUE)
            }
        },
        mounted() {
            if(this.curInstitute > 0) {
                this.loadInstituteDepartments(this.curInstitute);
            }
        },
        methods: {
            ...BX.Vuex.mapActions(['loadInstituteDepartments']),
            updateCurrentInstitute: function (event) {
                let institute = event.target.value;
                this.curInstitute = institute;
                this.loadInstituteDepartments(institute);
            },
            updateCurrentDepartment: function(event) {
                this.curDepartment = event.target.value;
            },
            updateFormField: function(event) {
                this.$store.commit('updateCurrent', {
                    value: event.target.value,
                    target: event.target.attributes.name.value
                });
            }
        },
        template: `
<div>
    <p class="text-center h3 text-muted mb-1">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_REQUEST_TITLE }}</p>
    <p class="text-danger mb-3">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_REQUIRED_HINT }}</p>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_INSTITUTE_TITLE }}</span>
        </div>
        <select class="form-select col-auto focus-bg-secondary focus-border-secondary"
            :disabled="!getCanEdit"
            name="institute"
            required
            @change="updateCurrentInstitute">
            <option value="0">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED }}</option>
            <option
                v-for="inst in getInstitutes"
                :value="inst.ID"
                :selected="inst.ID==curInstitute">
                    {{ inst.NAME }}
            </option>
        </select>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_DEPARTMENT_TITLE }}</span>
        </div>
        <input type="text"
            class="form-control col-auto"
            disabled
            :value="localize.MIREA_DOCREQUESTS_SUBSTITUTION_SELECT_INSTITUTE"
            v-if="curInstitute===0">
        <input type="text"
            class="form-control col-auto"
            disabled
            :value="localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOT_FOUND"
            v-else-if="getDepartments.length == 0">
        <select class="form-select col-auto focus-bg-secondary focus-border-secondary"
            name="department"
            :disabled="!getCanEdit"
            @change="updateCurrentDepartment"
            v-else>
            <option value="0">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED }}</option>
            <option
                v-for="dept in getDepartments"
                :value="dept.ID"
                :selected="dept.ID==curDepartment">
                    {{ dept.NAME }}
            </option>
        </select>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_POSITION_TITLE }}</span>
        </div>
        <input type="text"
            class="form-control col-auto"
            disabled
            :value="disabledText"
            v-if="curInstitute == 0 || curDepartment == 0">
        <textarea class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="position"
            :disabled="!getCanEdit"
            @input="updateFormField"
            v-else>{{ getCurrentData.position }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TERM_TITLE }}</div>
        <input type="text"
            class="form-control col-auto"
            disabled
            :value="disabledText"
            v-if="curInstitute == 0 || curDepartment == 0">
        <input type="text"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="term"
            :disabled="!getCanEdit"
            @input="updateFormField"
            :value="getCurrentData.term"
            v-else>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_WORK_PART_TITLE }}</span>
        </div>
        <input type="text"
            class="form-control col-auto"
            disabled
            :value="disabledText"
            v-if="curInstitute == 0 || curDepartment == 0">
        <input type="number"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="work_part"
            min="0.1"
            step="0.1"
            :disabled="!getCanEdit"
            @input="updateFormField"
            :value="getCurrentData.work_part"
            v-else>
    </div>
</div>`,
    }
);