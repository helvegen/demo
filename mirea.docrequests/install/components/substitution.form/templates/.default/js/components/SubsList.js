BX.Vue.component(
    'bx-docrequests-subs-list',
    {
        computed: {
            ...BX.Vuex.mapGetters(['localize', 'getCurrentData', 'getTableData', 'getCanEdit']),
            yearsOld: function () {
                let res = '';

                if(this.getTableData.birthday.length > 0) {
                    let old = (new Date().getTime() - new Date(this.getTableData.birthday)) / (24 * 3600 * 365.25 * 1000);
                    res = Math.floor(old);
                }
                return res;
            },
            hiddenFields: function() {
                let cur = this.getCurrentData;
                return {
                    names: Object.keys(cur),
                    values: Object.values(cur)
                };
            }
        },
        methods: {
            updateTableField: function(event) {
                this.$store.commit('updateTable', {
                    value: event.target.value,
                    target: event.target.attributes.name.value
                });
            }
        },
        template: `
<div>
    
    <input type="hidden"
        v-for="(name, i) in hiddenFields.names"
        :name="name"
        :value="hiddenFields.values[i]">
    
    <p class="text-center h3 text-muted mb-1">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TABLE_TITLE }}</p>
    <p class="text-danger mb-1">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_REQUIRED_HINT }}</p>
    <p class="text-danger mb-3">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NO_VALUE_HINT }}</p>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_FIO_TITLE }}</span>
        </div>
        <input type="text"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="fio"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.fio"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_FIO_PLACEHOLDER">
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_BIRTHDAY_TITLE }}</span>
        </div>
        <input type="date"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="birthday"
            required
            :disabled="!getCanEdit"
            @blur="updateTableField"
            :value="getTableData.birthday">
        <div class="input-group-text col-4">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_YEARS_OLD_TITLE }}: {{ yearsOld }}</div>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_POSITION_TITLE }}</span>
        </div>
        <input type="text"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="curPosition"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.curPosition"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOW">
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_WORK_PART_TITLE }}</span>
        </div>
        <input type="number"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="cur_work_part"
            required
            min="0"
            step="0.1"
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.cur_work_part"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOW">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-text">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_LAST_TITLE }}</div>
        <input type="date"
            class="form-control focus-bg-secondary focus-border-secondary"
            name="last"
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.last">
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_WORK_ORDER_TITLE }}</span>
        </div>
        <input type="text"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="work_order"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.work_order"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_WORK_ORDER_PLACEHOLDER">
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_BASE_EDU_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="base_edu"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_BASE_EDU_PLACEHOLDER">{{ getTableData.base_edu }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_LEVEL_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="science_level"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_LEVEL_PLACEHOLDER">{{ getTableData.science_level }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SEC_EDU_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="sec_edu"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_SEC_EDU_PLACEHOLDER">{{ getTableData.sec_edu }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_QUOLIF_UP_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="quolif_up"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_QUOLIF_UP_PLACEHOLDER">{{ getTableData.quolif_up }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_EDU_DISCIPLINS_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="edu_disciplins"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_EDU_DISCIPLINS_PLACEHOLDER">{{ getTableData.edu_disciplins }}</textarea>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-text">
            <span class="label-required pr-2">
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_PUB_NUM_TITLE }}
                <abbr class="ml-1"
                    :title="localize.MIREA_DOCREQUESTS_SUBSTITUTION_PUB_NUM_ABBR">
                    <i class="fa fa-question-circle text-secondary" aria-hidden="true"></i>
                </abbr>
            </span>
        </div>
        <input type="number"
            class="form-control focus-bg-secondary focus-border-secondary"
            name="pub_num"
            required
            min="0"
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.pub_num"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_PUB_NUM_PLACEHOLDER">
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_ARTICLES_TITLE }}
        </div>
        <div class="col col-mb-3 p-0">
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_ARTICLES_RUS }}</span>
            </div>
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_ARTICLES_VAK }}</span>
            </div>
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_ARTICLES_SCOPUS }}</span>
            </div>
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_ARTICLES_THT }}</span>
            </div>
            <div class="input-group-text text-left align-items-stretch border-radius-0">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SCIENCE_ARTICLES_RTZH }}</span>
            </div>
        </div>
        <div class="col p-0">
            <input type="number"
                class="form-control border-radius-tl-0 border-radius-bl-0 border-radius-br-0 mb--1px focus-bg-secondary focus-border-secondary"
                name="science_articles_rus"
                required
                min="0"
                :disabled="!getCanEdit"
                @input="updateTableField"
                :value="getTableData.science_articles_rus">
            <input type="number"
                class="form-control border-radius-0 mb--1px focus-bg-secondary focus-border-secondary"
                name="science_articles_vak"
                required
                min="0"
                :disabled="!getCanEdit"
                @input="updateTableField"
                :value="getTableData.science_articles_vak">
            <input type="number"
                class="form-control border-radius-tl-0 border-radius-bl-0 border-radius-tr-0 mb--1px focus-bg-secondary focus-border-secondary"
                name="science_articles_scopus"
                required
                min="0"
                :disabled="!getCanEdit"
                @input="updateTableField"
                :value="getTableData.science_articles_scopus">
            <input type="number"
                class="form-control border-radius-tl-0 border-radius-bl-0 border-radius-tr-0 mb--1px focus-bg-secondary focus-border-secondary"
                name="science_articles_tht"
                required
                min="0"
                :disabled="!getCanEdit"
                @input="updateTableField"
                :value="getTableData.science_articles_tht">
            <input type="number"
                class="form-control border-radius-tl-0 border-radius-bl-0 border-radius-tr-0 mb--1px focus-bg-secondary focus-border-secondary"
                name="science_articles_rtzh"
                required
                min="0"
                :disabled="!getCanEdit"
                @input="updateTableField"
                :value="getTableData.science_articles_rtzh">
        </div>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_INTL_SPEECH_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="intl_speech"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_INTL_SPEECH_PLACEHOLDER">{{ getTableData.intl_speech }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3">
            <span class="label-required pr-2">
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_PATENT_NUM_TITLE }}
                <abbr class="ml-1"
                    :title="localize.MIREA_DOCREQUESTS_SUBSTITUTION_PATENT_NUM_ABBR">
                    <i class="fa fa-question-circle text-secondary" aria-hidden="true"></i>
                </abbr>
            </span>
        </div>
        <input type="number"
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="patent_num"
            required
            min="0"
            :disabled="!getCanEdit"
            @input="updateTableField"
            :value="getTableData.patent_num"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_PATENT_NUM_PLACEHOLDER">
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_DH_NUM_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="dh_num"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_DH_NUM_PLACEHOLDER">{{ getTableData.dh_num }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_COUNCIL_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="membership_council"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_COUNCIL_PLACEHOLDER">{{ getTableData.membership_council }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_TITLE }}
        </div>
        <div class="col col-mb-3 p-0">
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_RINC }}</span>
            </div>
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">
                    {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_EXPERT }}
                    <abbr class="ml-1"
                        :title="localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_EXPERT_ABBR">
                        <i class="fa fa-question-circle text-secondary" aria-hidden="true"></i>
                    </abbr>
                </span>
            </div>
            <div class="input-group-text text-left align-items-stretch border-radius-0 mb--1px">
                <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_MEMBERSHIP_THT_RTZH }}</span>
            </div>
        </div>
        <div class="col p-0">
            <div class="form-control border-radius-tl-0 border-radius-bl-0 border-radius-br-0 mb--1px focus-bg-secondary focus-border-secondary">
                <div class="form-check-inline">
                    <input type="radio"
                        class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                        name="membership_rinc"
                        id="membership_rinc"
                        required
                        value="Y"
                        :disabled="!getCanEdit"
                        @change="updateTableField"
                        :checked="getTableData.membership_rinc==='Y'">
                    <label class="form-check-label" for="membership_rinc">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_YES }}
                    </label>
                </div>
                <div class="form-check-inline">
                    <input type="radio"
                        class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                        name="membership_rinc"
                        id="no-membership_rinc"
                        required
                        value="N"
                        :disabled="!getCanEdit"
                        @change="updateTableField"
                        :checked="getTableData.membership_rinc==='N'">
                    <label class="form-check-label" for="no-membership_rinc">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NO }}
                    </label>
                </div>
            </div>
            <div class="form-control border-radius-0 mb--1px focus-bg-secondary focus-border-secondary">
                <div class="form-check-inline">
                    <input type="radio"
                        class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                        name="membership_expert"
                        id="membership_expert"
                        required
                        value="Y"
                        :disabled="!getCanEdit"
                        @change="updateTableField"
                        :checked="getTableData.membership_expert==='Y'">
                    <label class="form-check-label" for="membership_expert">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_YES }}
                    </label>
                </div>
                <div class="form-check-inline">
                    <input type="radio"
                        class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                        name="membership_expert"
                        id="no-membership_expert"
                        required
                        value="N"
                        :disabled="!getCanEdit"
                        @change="updateTableField"
                        :checked="getTableData.membership_expert==='N'">
                    <label class="form-check-label" for="no-membership_expert">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NO }}
                    </label>
                </div>
            </div>
            <div class="form-control border-radius-tl-0 border-radius-bl-0 border-radius-tr-0 mb--1px focus-bg-secondary focus-border-secondary">
                <div class="form-check-inline">
                    <input type="radio"
                        class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                        name="membership_tht_rtzh"
                        id="membership_tht_rtzh"
                        required
                        value="Y"
                        :disabled="!getCanEdit"
                        @change="updateTableField"
                        :checked="getTableData.membership_tht_rtzh==='Y'">
                    <label class="form-check-label" for="membership_tht_rtzh">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_YES }}
                    </label>
                </div>
                <div class="form-check-inline">
                    <input type="radio"
                        class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                        name="membership_tht_rtzh"
                        id="no-membership_tht_rtzh"
                        required
                        value="N"
                        :disabled="!getCanEdit"
                        @change="updateTableField"
                        :checked="getTableData.membership_tht_rtzh==='N'">
                    <label class="form-check-label" for="no-membership_tht_rtzh">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NO }}
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_WORK_INVITE_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="work_invite"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_WORK_INVITE_PLACEHOLDER">{{ getTableData.work_invite }}</textarea>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_EDU_COURSE_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="edu_course"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_EDU_COURSE_PLACEHOLDER">{{ getTableData.edu_course }}</textarea>
    </div>
    <div class="input-group d-flex align-items-center mb-3">
        <div class="input-group-text">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_REWARD_TITLE }}</span>
        </div>
        <div class="form-control">
            <div class="form-check-inline">
                <input type="radio"
                    class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                    name="reward"
                    id="reward"
                    required
                    value="Y"
                    :disabled="!getCanEdit"
                    @change="updateTableField"
                    :checked="getTableData.reward==='Y'">
                <label class="form-check-label" for="reward">
                    {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_HAVE }}
                </label>
            </div>
            <div class="form-check-inline">
                <input type="radio"
                    class="form-check-input focus-border-secondary mt-1 mr-1 checked-success"
                    name="reward"
                    id="no-reward"
                    required
                    value="N"
                    :disabled="!getCanEdit"
                    @change="updateTableField"
                    :checked="getTableData.reward==='N'">
                <label class="form-check-label" for="no-reward">
                    {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NO }}
                </label>
            </div>
        </div>
    </div>
    <div class="input-group d-flex mb-3">
        <div class="input-group-text col-3 shy text-left align-items-stretch">
            <span class="label-required pr-2">{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_EXP_TITLE }}</span>
        </div>
        <textarea
            class="form-control col-auto focus-bg-secondary focus-border-secondary"
            name="exp"
            required
            :disabled="!getCanEdit"
            @input="updateTableField"
            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_EXP_PLACEHOLDER">{{ getTableData.exp }}</textarea>
    </div>
</div>`
    }
);