BX.Vue.component(
    'bx-docrequests-subs-form',
    {
        computed: {
            ...BX.Vuex.mapGetters([
                'localize',
                'getTabList',
                'getCurrentData',
                'getTableData',
                'getCanSubmit',
                'getFormResult',
                'getCanApprove',
                'getCanEdit'
            ]),
            tabNumber: {
                get: function () {
                    let arTabs = this.getTabList;
                    let reg = new RegExp('/');
                    let str = this.$route.path.replace(reg, '');
                    let num = arTabs.indexOf(str);
                    return parseInt((num === -1) ? 0 : num);
                },
                set: function () {
                    return true; // затычка
                }
            },
            checkFields: function() {
                return (
                    !!this.getCurrentData.institute &&
                    !!this.getCurrentData.department &&
                    !!this.getCurrentData.position &&
                    !!this.getCurrentData.work_part
                );
            }
        },
        mounted() {},
        data() {
            return {
                showComment: false,
                comment: ''
            }
        },
        methods: {
            ...BX.Vuex.mapActions(['loadFormResult', 'loadFormAction']),
            sendForm: function() {
                let cur = this.getCurrentData;
                let table = this.getTableData;
                this.loadFormResult(Object.assign(cur, table));
            },
            sendFormAction: function(event) {
                this.loadFormAction({
                    action: event.target.attributes.formAction.value,
                    id: this.getCurrentData.id,
                    comment: this.comment,
                    list: this.getCurrentData.list
                });
            },
            addComment: function() {
                this.showComment = true;
            }
        },
        template: `
<div>
    <div class="alert alert-success" v-if="getFormResult.result === 'OK'">
        <p>{{ getFormResult.message }}</p>
        <div>
            <a href="/docrequests" class="btn btn-sm btn-success mr-3">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TO_LIST }}
            </a>
            <a href="/docrequests/add" class="btn btn-sm btn-success mr-3">
                <i class="fa fa-plus" aria-hidden="true"></i>
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TO_NEW }}
            </a>
        </div>
    </div>
    
    <div class="alert alert-danger" v-else-if="getFormResult.result === 'ERROR'">
        <p>{{ getFormResult.message }}</p>
        <div>
            <a href="/docrequests" class="btn btn-sm btn-danger mr-3">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TO_LIST }}
            </a>
            <a href="/docrequests/add" class="btn btn-sm btn-danger mr-3">
                <i class="fa fa-plus" aria-hidden="true"></i>
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TO_NEW }}
            </a>
        </div>
    </div>

    <div class="row" v-else>
        <div class="tab-nav mb-4 col-2 order-2">
            <router-link
                v-for="(tabCode, index) in getTabList"
                :to="index===0 ? '/' : tabCode" exact
                :key="index">
                    <button class="btn btn-outline-secondary d-block col-12 mb-3 shy" 
                        :index="index"
                        :id="tabCode"
                        :disabled="!checkFields && index>tabNumber"
                        lang="ru">
                            {{ localize['MIREA_DOCREQUESTS_SUBSTITUTION_TAB_TITLE_'+tabCode] }}
                    </button>
            </router-link>
        </div>
        
        <div class="tab-content col-10 order-1">
            <router-view></router-view>
            
            <div class="tab-buttons mt-4">
                
                <div class="form-group"
                    v-if="showComment">
                        <textarea class="form-control mb-2"
                            :placeholder="localize.MIREA_DOCREQUESTS_SUBSTITUTION_ENTER_COMMENT"
                            v-model="comment"></textarea>
                        <button class="btn btn-sm btn-success"
                            formAction="cancel"
                            v-if="!!comment"
                            @click.prevent="sendFormAction">
                            {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_SEND_CANCEL }}
                        </button>
                </div>
                <div v-else>
                    <router-link
                        :to="this.tabNumber === 1 ? '/' : this.getTabList[this.tabNumber-1]"
                        class="mr-3 btn btn-primary btn-sm"
                        v-if="this.tabNumber > 0">
                            <span :index="this.tabNumber-1">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                {{localize.MIREA_DOCREQUESTS_SUBSTITUTION_BACK_TITLE}}
                            </span>
                    </router-link>
                    <router-link
                        :to="this.getTabList[this.tabNumber+1]"
                        class="mr-3 btn btn-primary btn-sm"
                        v-if="(this.tabNumber+1 < this.getTabList.length) && checkFields">
                            <span :index="this.tabNumber+1">
                                {{localize.MIREA_DOCREQUESTS_SUBSTITUTION_FORWARD_TITLE}}
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </span>
                    </router-link>
                    <a href="/docrequests/" class="btn btn-sm btn-outline-primary mr-3">{{localize.MIREA_DOCREQUESTS_SUBSTITUTION_TO_LIST}}</a>
                    <span class="btn btn-outline-danger btn-sm"
                        v-if="(this.tabNumber+1 < this.getTabList.length) && !checkFields">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOT_FILLED }}
                    </span>
                    <input type="submit"
                        class="btn btn-sm btn-success mr-3"
                        :value="localize.MIREA_DOCREQUESTS_SUBSTITUTION_SUBMIT_TITLE"
                        @click.prevent="sendForm"
                        v-if="(this.tabNumber+1 == this.getTabList.length) && getCanSubmit && getCanEdit">
                    <span class="btn btn-outline-danger btn-sm mr-3"
                        v-if="(this.tabNumber+1 == this.getTabList.length) && !getCanSubmit">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOT_FILLED }}
                    </span>
                    <span class="btn btn-outline-danger btn-sm mr-3"
                        v-if="!getCanApprove && !getCanEdit">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_READONLY }}
                    </span>
                    <span class="btn btn-success btn-sm mr-3"
                        v-if="(this.tabNumber+1 == this.getTabList.length) && getCanApprove && !getCanEdit"
                        formAction="approve"
                        @click="sendFormAction">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_APPROVE }}
                    </span>
                    <span class="btn btn-danger btn-sm mr-3"
                        v-if="(this.tabNumber+1 == this.getTabList.length) && getCanApprove && !getCanEdit"
                        @click="addComment">
                        {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_CANCEL }}
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>`
    }
);