const TabRequest = {
    template: '<bx-docrequests-subs-request/>',
    beforeRouteLeave(to, from, next) {
        let cur = store.getters.getCurrentData;
        if(
            !!cur.institute &&
            !!cur.department &&
            !!cur.position &&
            !!cur.work_part
        ) {
            next();
        } else {
            next(false);
        }
    },
};
const TabList = {
    template: '<bx-docrequests-subs-list/>',
    beforeRouteEnter(to, from, next) {
        let cur = store.getters.getCurrentData;
        if(
            !!cur.institute &&
            !!cur.department &&
            !!cur.position &&
            !!cur.work_part
        ) {
            next();
        } else {
            next('/');
        }
    }
};

const routes = [
    {path: '/', component: TabRequest},
    {path: '/list', component: TabList}
];

const router = BX.VueRouter.create({routes});

BX.ready(() => {
    BX.Vue.create({
        el: '#mirea_docrequests_substitution_form_main',
        store,
        router,
        template: `<bx-docrequests-subs-form/>`
    });
});
