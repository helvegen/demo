<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Mirea\DocRequests\Config;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\HttpApplication;
use \Bitrix\Main\Web\Json;

Loc::loadMessages(__FILE__);

class SubstitutionFormComponent extends CBitrixComponent implements Controllerable
{
    public function __construct($component = null)
    {
        parent::__construct($component);
        Loader::includeModule('mirea.docrequests');

        $obConfig = new Config();
        $obConfig->CheckReferences();
    }

    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadLanguageFile(__FILE__);
    }

    public function executeComponent()
    {
        try {
            $GLOBALS['APPLICATION']->SetTitle(Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_FORM_PAGE_TITLE'));
            $this->getResult();

            $this->includeComponentTemplate();
        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

    public function getResult()
    {
        global $USER;
        $arUser = CUser::GetByID($USER->GetID())->Fetch();

        $obConfig = new Config();
//        echo '<pre>'; print_r($obConfig); echo '</pre>';
        $arResult = array(
            'MODULE_CONFIG' => array(
                'MODULE_PREFIX' => $obConfig->MODULE_PREFIX,
                'MODULE_ROOT' => $obConfig->MODULE_ROOT
            )
        );

        // текущая заявка
        $arData = array();
        $arTable = array();
        $request = HttpApplication::getInstance()->getContext()->getRequest();
        if ($request->getQuery('req')) {
            $arRequest = CIBlockElement::GetByID($request->getQuery('req'))->GetNext();
            $arProps = array();
            CIBlockElement::GetPropertyValuesArray(
                $arProps,
                $arRequest['IBLOCK_ID'],
                array('ID' => $arRequest['ID'])
            );
            $arProps = $arProps[$arRequest['ID']];
            $arData = array(
                'ID' => $arRequest['ID'],
                'NAME' => $arRequest['NAME'],
                'CREATED_BY' => array(
                    'ID' => $arRequest['CREATED_BY'],
                    'NAME' => Config::GetUserName($arRequest['CREATED_BY'])
                ),
                'DATE_CREATE' => date('d.m.Y', strtotime($arRequest['DATE_CREATE'])),
                'REQUEST_TIME' => $arProps['MIREA_DOCREQUESTS_REQUEST_TIME']['VALUE'],
                'WORK_PART' => $arProps['MIREA_DOCREQUESTS_WORK_PART']['VALUE'],
                'INSTITUTE' => $arProps['MIREA_DOCREQUESTS_INSTITUTE']['VALUE'],
                'DEPARTMENT' => $arProps['MIREA_DOCREQUESTS_DEPARTMENT']['VALUE']
                // статусы
            );

            $ar = CIBlockElement::GetByID($arProps['MIREA_DOCREQUESTS_WORK_LIST']['VALUE'])->GetNext();
            $arTable = Json::decode(html_entity_decode($ar['DETAIL_TEXT']));
            $arTable['NAME'] = $ar['NAME'];
            $arTable['ID'] = $ar['ID'];

            // доступ к редактированию
            if (
                $USER->GetID() == $arRequest['CREATED_BY'] and
                $arProps['MIREA_DOCREQUESTS_REQUEST_STATUS']['VALUE_XML_ID'] == 'mirea_docrequests_status_cancelled'
            ) {
                $arResult['CAN_EDIT'] = true;
            } else {
                $arResult['CAN_EDIT'] = false;
            }
            // доступ к редактированию - конец

            // доступ к согласованию
            // директор института
            $bApprove = false;
            if (
                $arProps['MIREA_DOCREQUESTS_REQUEST_STATUS']['VALUE_XML_ID'] == 'mirea_docrequests_status_approve_director' and
                in_array($arProps['MIREA_DOCREQUESTS_INSTITUTE']['VALUE'], $arUser['UF_DEPARTMENT']) and
                $arProps['MIREA_DOCREQUESTS_APPROVED_DIRECTOR']['VALUE_XML_ID'] != 'mirea_docrequests_approved_director_yes'
            ) {
                $bApprove = true;
            }
            // ответственные
            if (
                $arProps['MIREA_DOCREQUESTS_REQUEST_STATUS']['VALUE_XML_ID'] == 'mirea_docrequests_status_approve_responsible' and
                in_array($obConfig->moduleOptions['mirea_docrequests_resp_1_user']['value'], $USER->GetUserGroupArray()) and
                !in_array($arUser['ID'], $arProps['MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE']['VALUE'])
            ) {
                $bApprove = true;
            }
            // ректор
            if (
                $arProps['MIREA_DOCREQUESTS_REQUEST_STATUS']['VALUE_XML_ID'] == 'mirea_docrequests_status_approve_final' and
                $arUser['ID'] == $obConfig->moduleOptions['mirea_docrequests_resp_2_user']['value'] and
                $arProps['MIREA_DOCREQUESTS_APPROVED_FINAL']['VALUE_XML_ID'] != 'mirea_docrequests_approved_final_yes'
            ) {
                $bApprove = true;
            }
            $arResult['CAN_APPROVE'] = $bApprove;
            // доступ к согласованию - конец
        }

        $arResult['REQUEST'] = $arData;
        $arResult['LIST'] = $arTable;
        // текущая заявка - конец

        // институты
        $res = array();
        $arTree = CIntranetUtils::GetDeparmentsTree();
        $arPattern = explode(',', $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_institute_marker']['value']);
        $arPattern = array_map(
            function ($s) {
                return '(' . trim(strtolower($s)) . ')';
            },
            $arPattern
        );
        $sPattern = '/' . implode('|', $arPattern) . '/ui';
        foreach ($arTree as $k => $v) {
            $ar = array_filter(
                CIntranetUtils::GetDepartmentsData($v),
                function ($s) use ($sPattern) {
                    $res = preg_match(
                        $sPattern,
                        strtolower($s)
                    );
                    return $res === 1;
                }
            );
            if ($ar) {
                $res[] = $k;
            }
        }
        $arResult['INSTITUTES'] = $this->getFormattedDepartments($res);
        // институты - конец

        $this->arResult = $arResult;
    }

    public function configureActions()
    {
        return array(
            'loadInstituteDepartments' => array(
                'prefilters' => [
                    new ActionFilter\Authentication
                ],
            ),
            'saveForm' => array(
                'prefilters' => [
                    new ActionFilter\Authentication
                ],
            ),
            'changeStatus' => array(
                'prefilters' => [
                    new ActionFilter\Authentication
                ],
            ),
        );
    }

    public function loadInstituteDepartmentsAction($data)
    {
        $arReturn = array();

        if ($data) {
            $arTree = CIntranetUtils::GetDeparmentsTree($data);
            $obConfig = new Config();
            $arPattern = explode(',', $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_institute_marker']['value']);
            $arPattern = array_map(
                function ($s) {
                    return '(' . trim(strtolower($s)) . ')';
                },
                $arPattern
            );
            $sPattern = '/' . implode('|', $arPattern) . '/ui';

            $arReturn = array_filter(
                $this->getFormattedDepartments($arTree[$data]),
                function ($ar) use ($sPattern) {
                    $res = $res = preg_match(
                        $sPattern,
                        strtolower($ar['NAME'])
                    );
                    return $res === 1;
                }
            );
        }

        return $arReturn;
    }

    public function saveFormAction($data)
    {
        $arReturn = array();

        $obConfig = new Config();

        if (
            $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_lists_iblock']['value'] and
            $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_requests_iblock']['value']
        ) {
            // лист эффективности
            $arTableFields = array(
                'curPosition',
                'cur_work_part',
                'work_order',
                'birthday',
                'base_edu',
                'science_level',
                'sec_edu',
                'quolif_up',
                'edu_disciplins',
                'pub_num',
                'science_articles_rus',
                'science_articles_vak',
                'science_articles_scopus',
                'science_articles_tht',
                'science_articles_rtzh',
                'intl_speech',
                'patent_num',
                'dh_num',
                'membership_council',
                'membership_rinc',
                'membership_expert',
                'membership_tht_rtzh',
                'work_invite',
                'edu_course',
                'reward',
                'exp',
            );
            $ar = array();
            foreach ($arTableFields as $field) {
                $ar[$field] = $data[$field];
            }
            $arFields = array(
                'IBLOCK_ID' => $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_lists_iblock']['value'],
                'NAME' => $data['fio'],
                'ACTIVE' => 'Y',
                'DETAIL_TEXT' => Json::encode($ar)
            );
            $el = new CIBlockElement();
            if($data['list']) {
                $el->Update($data['list'], $arFields);
                $listID = $data['list'];
            } else {
                $listID = $el->Add($arFields);
            }
            // лист эффективности - конец

            if ($listID) {
                // заявка
                $arFields = array(
                    'IBLOCK_ID' => $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_requests_iblock']['value'],
                    'ACTIVE' => 'Y',
                    'NAME' => $data['position'],
                    'PROPERTY_VALUES' => array(
                        'MIREA_DOCREQUESTS_REQUEST_TIME' => $data['term'],
                        'MIREA_DOCREQUESTS_WORK_PART' => $data['work_part'],
                        'MIREA_DOCREQUESTS_INSTITUTE' => array(
                            'VALUE' => $data['institute'],
                            'DESCRIPTION' => CIntranetUtils::GetDepartmentsData([$data['institute']])[$data['institute']]
                        ),
                        'MIREA_DOCREQUESTS_DEPARTMENT' => array(
                            'VALUE' => $data['department'],
                            'DESCRIPTION' => CIntranetUtils::GetDepartmentsData([$data['department']])[$data['department']]
                        ),
                        'MIREA_DOCREQUESTS_REQUEST_STATUS' => $obConfig->moduleEnums['mirea_docrequests_status_approve_director']['ID'],
                        'MIREA_DOCREQUESTS_WORK_LIST' => $listID,
                        'MIREA_DOCREQUESTS_CANCELLED_COMMENT' => false
                    )
                );
                $el = new CIBlockElement();
                if($data['id']) {
                    $el->Update($data['id'], $arFields);
                    $requestID = $data['id'];
                } else {
                    $requestID = $el->Add($arFields);
                }
                // заявка - конец

                if ($requestID) {
                    $arReturn['TYPE'] = 'OK';
                    $arReturn['MESSAGE'] = Loc::getMessage('MIREA_DOCREQUESTS_ADD_REQUEST_OK');
                    $arReturn['ID'] = $requestID;

                    // пушка директору института
                    $toSend = array(
                        'TO_USER_ID' => CIntranetUtils::GetDepartmentManagerID($data['institute']),
                        'FROM_USER_ID' => $GLOBALS['USER']->GetID(),
                        'MESSAGE_TYPE' => 'S',
                        'NOTIFY_MODULE' => 'im',
                        'NOTIFY_MESSAGE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_NEED_APPROVE') . ': ' .
                            "<a href='/docrequests/$requestID/'>{$data['position']}</a>"
                    );
                    $msg = new \CIMMessenger();
                    $msg->Add($toSend);

                } else {
                    $arReturn['TYPE'] = 'ERROR';
                    $arReturn['MESSAGE'] = strip_tags($el->LAST_ERROR) . ' ' .
                        Loc::getMessage('MIREA_DOCREQUESTS_CONTACT_ADMIN');
                }

            } else {
                $arReturn['TYPE'] = 'ERROR';
                $arReturn['MESSAGE'] = strip_tags($el->LAST_ERROR) . ' ' .
                    Loc::getMessage('MIREA_DOCREQUESTS_CONTACT_ADMIN');
            }

        } else {
            $arReturn['TYPE'] = 'ERROR';
            $arReturn['MESSAGE'] = Loc::getMessage('MIREA_DOCREQUESTS_NO_MODULE_OPTIONS') . ' ' .
                Loc::getMessage('MIREA_DOCREQUESTS_CONTACT_ADMIN');
        }

        return $arReturn;
    }

    public function changeStatusAction($data)
    {
        $arReturn = array();

        if ($data['id']) {
            global $USER;
            $curUser = $USER->GetID();
            $arUser = CUser::GetByID($curUser)->Fetch();
            $obConfig = new Config();
            $arElem = CIBlockElement::GetByID($data['id'])->Fetch();
            $arProps = array();
            CIBlockElement::GetPropertyValuesArray(
                $arProps,
                $arElem['IBLOCK_ID'],
                array('ID' => $arElem['ID'])
            );
            $arProps = $arProps[$arElem['ID']];

            if ($data['action'] == 'cancel' and $data['comment']) { // отклонить
                CIBlockElement::SetPropertyValuesEx(
                    $arElem['ID'],
                    $arElem['IBLOCK_ID'],
                    array(
                        'MIREA_DOCREQUESTS_REQUEST_STATUS' => $obConfig->moduleEnums['mirea_docrequests_status_cancelled']['ID'],
                        'MIREA_DOCREQUESTS_CANCELLED_COMMENT' => $data['comment'],
                        'MIREA_DOCREQUESTS_APPROVED_DIRECTOR' => false,
                        'MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE' => false,
                        'MIREA_DOCREQUESTS_APPROVED_FINAL' => false
                    )
                );
                $el = new CIBlockElement();
                $el->Update(
                    $arElem['ID'],
                    array('MODIFIED_BY' => $curUser)
                );

                // пушка создателю заявки
                $toSend = array(
                    'TO_USER_ID' => $arElem['CREATED_BY'],
                    'FROM_USER_ID' => $curUser,
                    'MESSAGE_TYPE' => 'S',
                    'NOTIFY_MODULE' => 'im',
                    'NOTIFY_MESSAGE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_CANCELLED') . ' ' .
                        Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_SHOW') . ' ' .
                        "<a href='/docrequests/{$arElem['ID']}/'>{$arElem['NAME']}</a>"
                );
                $msg = new \CIMMessenger();
                $msg->Add($toSend);

                $arReturn['TYPE'] = 'OK';
                $arReturn['MESSAGE'] = Loc::getMessage('MIREA_DOCREQUESTS_CANCEL_REQUEST_OK');
                $arReturn['ID'] = $arElem['ID'];
            } elseif ($data['action'] == 'approve') { // согласовать
                $arToSet = array();
                $arToNext = array();

                // директор
                if(
                    $arProps['MIREA_DOCREQUESTS_REQUEST_STATUS']['VALUE_XML_ID'] == 'mirea_docrequests_status_approve_director' and
                    in_array($arProps['MIREA_DOCREQUESTS_INSTITUTE']['VALUE'], $arUser['UF_DEPARTMENT']) and
                    $arProps['MIREA_DOCREQUESTS_APPROVED_DIRECTOR']['VALUE_XML_ID'] != 'mirea_docrequests_approved_director_yes'
                ) {
                    $arToSet = array(
                        'MIREA_DOCREQUESTS_REQUEST_STATUS' => $obConfig->moduleEnums['mirea_docrequests_status_approve_responsible']['ID'],
                        'MIREA_DOCREQUESTS_APPROVED_DIRECTOR' => $obConfig->moduleEnums['mirea_docrequests_approved_director_yes']['ID'],
                    );
                    $arToNext = CGroup::GetGroupUser($obConfig->moduleOptions['mirea_docrequests_resp_1_user']['value']);
                }

                // ответственные
                if(
                    $arProps['MIREA_DOCREQUESTS_REQUEST_STATUS']['VALUE_XML_ID'] == 'mirea_docrequests_status_approve_responsible' and
                    in_array($obConfig->moduleOptions['mirea_docrequests_resp_1_user']['value'], $USER->GetUserGroupArray()) and
                    !in_array($arUser['ID'], $arProps['MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE']['VALUE'])
                ) {

                    $arResps = $arProps['MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE']['VALUE'];
                    $arResps[] = $curUser;

                    if($arResps == CGroup::GetGroupUser($obConfig->moduleOptions['mirea_docrequests_resp_1_user']['value'])) { // все ответственные согласовали
                        $arToSet = array(
                            'MIREA_DOCREQUESTS_REQUEST_STATUS' => $obConfig->moduleEnums['mirea_docrequests_status_approve_final']['ID'],
                            'MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE' => $arResps
                        );
                        $arToNext = array($obConfig->moduleOptions['mirea_docrequests_resp_2_user']['value']); // оповещение ректору
                    } else { // не все ответственные согласовали
                        $arToSet = array(
                            'MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE' => $arResps
                        );
                        $arToNext = CGroup::GetGroupUser($obConfig->moduleOptions['mirea_docrequests_resp_1_user']['value']);
                        unset($arToNext[array_search($curUser, $arToNext)]); // оповещение всем остальным ответственным
                    }
                }

                // ректор
                if($curUser == $obConfig->moduleOptions['mirea_docrequests_resp_2_user']['value']) {
                    $arToSet = array(
                        'MIREA_DOCREQUESTS_REQUEST_STATUS' => $obConfig->moduleEnums['mirea_docrequests_status_approved']['ID'],
                        'MIREA_DOCREQUESTS_APPROVED_FINAL' => $obConfig->moduleEnums['mirea_docrequests_approved_final_yes']['ID']
                    );
                }

                // изменить статус
                CIBlockElement::SetPropertyValuesEx($arElem['ID'], $arElem['IBLOCK_ID'], $arToSet);
                $el = new CIBlockElement();
                $el->Update(
                    $arElem['ID'],
                    array('MODIFIED_BY' => $curUser)
                );

                // пушки следующим согласующим
                if($arToNext) {
                    foreach ($arToNext as $iToNext) {
                        $toSend = array(
                            'TO_USER_ID' => $iToNext,
                            'FROM_USER_ID' => $curUser,
                            'MESSAGE_TYPE' => 'S',
                            'NOTIFY_MODULE' => 'im',
                            'NOTIFY_MESSAGE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_NEED_APPROVE') . ': ' .
                                "<a href='/docrequests/{$arElem['ID']}/'>{$arElem['NAME']}</a>"
                        );
                        $msg = new \CIMMessenger();
                        $msg->Add($toSend);
                    }
                }

                // пушка юзеру
                $toSend = array(
                    'TO_USER_ID' => $arElem['CREATED_BY'],
                    'FROM_USER_ID' => $curUser,
                    'MESSAGE_TYPE' => 'S',
                    'NOTIFY_MODULE' => 'im',
                    'NOTIFY_MESSAGE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_USER_APPROVED') . ': ' .
                        Config::GetUserName($curUser) . '. ' .
                        Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_SHOW') . ' ' .
                        "<a href='/docrequests/{$arElem['ID']}/'>{$arElem['NAME']}</a>"
                );
                $msg = new \CIMMessenger();
                $msg->Add($toSend);

                $arReturn['TYPE'] = 'OK';
                $arReturn['MESSAGE'] = Loc::getMessage('MIREA_DOCREQUESTS_APPROVE_REQUEST_OK');
                $arReturn['ID'] = $arElem['ID'];
            } else {
                $arReturn['TYPE'] = 'ERROR';
                $arReturn['MESSAGE'] = Loc::getMessage('MIREA_DOCREQUESTS_NO_MODULE_OPTIONS') . ' ' .
                    Loc::getMessage('MIREA_DOCREQUESTS_CONTACT_ADMIN');
                $arReturn['ID'] = $arElem['ID'];
            }
        } else {
            $arReturn['TYPE'] = 'ERROR';
            $arReturn['MESSAGE'] = Loc::getMessage('MIREA_DOCREQUESTS_NO_MODULE_OPTIONS') . ' ' .
                Loc::getMessage('MIREA_DOCREQUESTS_CONTACT_ADMIN');
        }

        return $arReturn;
    }

    protected function getFormattedDepartments($data)
    {
        if (!is_array($data)) {
            $data = array($data);
        }

        $arReturn = array();
        $arData = CIntranetUtils::GetDepartmentsData($data);
        asort($arData);
        foreach ($arData as $k => $v) {
            $arReturn[] = array(
                'ID' => $k,
                'NAME' => $v
            );
        }

        return $arReturn;
    }
}

