<?php

$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_GRID_PAGE_TITLE'] = 'Список заявок';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_NO_MODULE_OPTIONS'] = 'Отсутствуют необходимые настройки модуля.';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_CONTACT_ADMIN'] = 'Обратитесь к администратору портала.';
