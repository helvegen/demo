const TabContent = {template: '<bx-docrequests-subs-table/>'};

const routes = [
    {
        path: '/',
        component: TabContent,
        props: true
    },
    {
        path: '/:dataType',
        component: TabContent,
        props: true
    },
];

const router = BX.VueRouter.create({routes});

BX.ready(() => {
    BX.Vue.create({
        el: '#mirea_docrequests_substitution_grid_main',
        store,
        router,
        template: `<bx-docrequests-subs-grid/>`
    });
});
