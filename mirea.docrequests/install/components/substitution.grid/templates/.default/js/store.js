const store = BX.Vuex.store({
    state: {
        tabList: [
            'myRequests',
            'toApprove',
            // 'archive'
        ],
        myRequests: [],
        toApprove: [],
        archive: []
    },
    getters: {
        localize() {return BX.Vue.getFilteredPhrases('MIREA_DOCREQUESTS_SUBSTITUTION_');},
        getTabList(state) {return state.tabList;},
        getMyRequests(state) {return state.myRequests;},
        getToApprove(state) {return state.toApprove;},
        getArchive(state) {return state.archive;},
    },
    actions: {},
    mutations: {}
});