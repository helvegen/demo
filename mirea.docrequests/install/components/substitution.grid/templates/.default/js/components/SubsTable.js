BX.Vue.component(
    'bx-docrequests-subs-table',
    {
        computed: {
            ...BX.Vuex.mapGetters(['localize', 'getMyRequests', 'getArchive', 'getToApprove']),
            tableData: function() {
                let res = [];

                if(!this.$route.params.dataType) {
                    res = this.getMyRequests;
                } else {
                    res = (this.$route.params.dataType==='toApprove') ? this.getToApprove : this.getArchive;
                }

                return res;
            }
        },
        props: ['dataType'],
        methods: {},
        template: `
<div>
    <table class="table mt-4 border-light table-hover" v-if="tableData.length > 0">
        <thead>
            <tr>
                <th>ID</th>
                <th>{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_REQUEST }}</th>
                <th>{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_CREATED_BY }}</th>
                <th>{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_STATUS }}</th>
                <th>{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_COMMENT }}</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in tableData">
                <td>{{ item.ID }}</td>
                <td>
                    <a :href="'/docrequests/' + item.ID">{{ item.NAME }}</a>
                </td>
                <td>
                    <span class="main-grid-cell-content" data-prevent-default="true">
                        <a :href="'/company/personal/user/' + item.CREATED_BY.ID + '/'">
                            {{ item.CREATED_BY.NAME }}
                        </a>
                    </span>
                </td>
                <td>{{ item.PROPERTIES.MIREA_DOCREQUESTS_REQUEST_STATUS.VALUE_LABEL }}</td>
                <td>
                    <span v-if="!!item.PROPERTIES.MIREA_DOCREQUESTS_CANCELLED_COMMENT.VALUE">
                        <span class="main-grid-cell-content" data-prevent-default="true">
                            <a :href="'/company/personal/user/' + item.MODIFIED_BY.ID + '/'">
                                {{ item.MODIFIED_BY.NAME }}
                            </a>
                        </span>:
                        {{ item.PROPERTIES.MIREA_DOCREQUESTS_CANCELLED_COMMENT.VALUE }}
                    </span>
                </td>
            </tr>
        </tbody>
    </table>
    
    <div class="alert alert-light" v-else>{{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_NOTHING_TO_SHOW }}</div>
</div>`
    }
);