BX.Vue.component(
    'bx-docrequests-subs-grid',
    {
        computed: {
            ...BX.Vuex.mapGetters([
                'localize', 'getTabList', 'getToApprove'
            ]),
        },
        mounted() {},
        methods: {
        },
        template: `
<div>
    <div class="tab-nav d-flex justify-content-between">
        <div>
            <router-link
                to="/" exact
                key="0">
                    <button class="btn btn-outline-secondary mr-3" 
                        index="0"
                        id="myRequests"
                        lang="ru">
                            {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TAB_TITLE_myRequests }}
                    </button>
            </router-link>
            <router-link
                to="/toApprove" exact
                key="1">
                    <button class="btn btn-outline-secondary mr-3" 
                        index="1"
                        id="toApprove"
                        lang="ru"
                        v-if="getToApprove.length > 0">
                            {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_TAB_TITLE_toApprove }}
                    </button>
            </router-link>
        </div>
        
        <div>
            <a href="/docrequests/add/" class="btn btn-primary">
                <i class="fa fa-plus" aria-hidden="true"></i>
                {{ localize.MIREA_DOCREQUESTS_SUBSTITUTION_ADD }}
            </a>
        </div>
    </div>

    <router-view></router-view>
</div>`
    }
);