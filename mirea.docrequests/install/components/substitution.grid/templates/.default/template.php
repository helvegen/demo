<?php

defined('B_PROLOG_INCLUDED') || die;

$sPrefix = "{$arResult['MODULE_CONFIG']['MODULE_PREFIX']}_substitution";
$bootstrapPath = $arResult['MODULE_CONFIG']['MODULE_ROOT'] . '/references/bootstrap-5.0.0-beta1';

CJSCore::RegisterExt(
    "{$sPrefix}_grid",
    array(
        'css' => array(
            '/local/libs/bootstrap/css/bootstrap.min.css', // костыль для отступов
            '/bitrix/css/main/font-awesome.css',
            $this->GetFolder() . '/css/style.css',
            $bootstrapPath . '/css/bootstrap.min.css',
        ),
        'js' => array(
            $this->GetFolder() . '/js/store.js',
            $this->GetFolder() . '/js/GridMain.js',
            $this->GetFolder() . '/js/components/SubsGrid.js',
            $this->GetFolder() . '/js/components/SubsTable.js',
        ),
        'rel' => array(
            'ui.vue',
            'ui.vue.vuex',
            'ui.vue.router'
        ),
        'lang' => array(
            $this->GetFolder() . "/lang/" . LANGUAGE_ID . "/js/componentPhrases.php",
        )
    )
);

//echo '<pre>'; print_r($arResult); echo '</pre>';

?>

<div id="<?= $sPrefix ?>_grid_main"></div>

<script>
    store.state.myRequests = <?=CUtil::PhpToJSObject(array_values($arResult['MY_REQUESTS']))?>;
    store.state.toApprove = <?=CUtil::PhpToJSObject(array_values($arResult['TO_APPROVE']))?>;
</script>

<?

CJSCore::Init(array("{$sPrefix}_grid"));
?>
