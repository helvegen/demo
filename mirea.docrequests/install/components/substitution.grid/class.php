<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Mirea\DocRequests\Config;

Loc::loadMessages(__FILE__);

class SubstitutionGridComponent extends CBitrixComponent
{
    public function __construct($component = null)
    {
        parent::__construct($component);
        Loader::includeModule('mirea.docrequests');

        $obConfig = new Config();
        $obConfig->CheckReferences();
    }

    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadLanguageFile(__FILE__);
    }

    public function executeComponent()
    {
        if($_REQUEST['req']) {
            $GLOBALS['APPLICATION']->IncludeComponent(
                'mirea.docrequests:substitution.form',
                '',
                array(
                    'ID' => $_REQUEST['req']
                )
            );
        } else {
            try {
                $GLOBALS['APPLICATION']->SetTitle(Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_GRID_PAGE_TITLE'));
                $this->getResult();

                $this->includeComponentTemplate();
            } catch (Exception $e) {
                ShowError($e->getMessage());
            }
        }
    }

    protected function getResult()
    {
        global $USER;

        $obConfig = new Config();
        $arResult = array(
            'MODULE_CONFIG' => array(
                'MODULE_PREFIX' => $obConfig->MODULE_PREFIX,
                'MODULE_ROOT' => $obConfig->MODULE_ROOT
            )
        );

        if ($obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_requests_iblock']['value']) {
            $IBlockRequests = $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_requests_iblock']['value'];
            $curUserID = $USER->GetID();

            // для текущего пользователя получить все его заявки
            $arResult['MY_REQUESTS'] = $this->getFormattedElements(
                array(
                    'IBLOCK_ID' => $IBlockRequests,
                    'CREATED_BY' => $curUserID
                ),
                $this->arParams['SORT_DEFAULT']
            );

            // сформировать список статусов для фильтра
            $iPropStatus = $obConfig->moduleProps[strtoupper($obConfig->MODULE_PREFIX) . '_REQUEST_STATUS']['ID'];
            $arStatusEnums = array_filter(
                $obConfig->moduleEnums,
                function ($ar) use ($iPropStatus) {
                    return $ar['PROPERTY_ID'] == $iPropStatus;
                }
            );
            $arStatuses = array_combine(
                array_keys($arStatusEnums),
                array_column($arStatusEnums, 'ID')
            );

            $arApprove = array();

            // ректор
            if ($curUserID == $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_2_user']['value']) {
                $arFilter = array(
                    'IBLOCK_ID' => $IBlockRequests,
                    'PROPERTY_MIREA_DOCREQUESTS_REQUEST_STATUS' => $arStatuses['mirea_docrequests_status_approve_final'],
                    'ACTIVE' => 'Y'
                );
                $arApprove = array_merge($arApprove, $this->getFormattedElements($arFilter));
            }

            // ответственные
            if (
                in_array(
                    $obConfig->moduleOptions[$obConfig->MODULE_PREFIX . '_resp_1_user']['value'],
                    CUser::GetUserGroup($curUserID)
                )
            ) {
                $arFilter = array(
                    'IBLOCK_ID' => $IBlockRequests,
                    'PROPERTY_MIREA_DOCREQUESTS_REQUEST_STATUS' => array(
                        $arStatuses['mirea_docrequests_status_approve_final'],
                        $arStatuses['mirea_docrequests_status_approve_responsible']
                    ),
                    'ACTIVE' => 'Y'
                );
                $arApprove = array_merge($arApprove, $this->getFormattedElements($arFilter));
            }

            // директор института
            $arFilter = array(
                'IBLOCK_ID' => $IBlockRequests,
                'PROPERTY_MIREA_DOCREQUESTS_REQUEST_STATUS' => array(
                    $arStatuses['mirea_docrequests_status_approve_final'],
                    $arStatuses['mirea_docrequests_status_approve_responsible'],
                    $arStatuses['mirea_docrequests_status_approve_director']
                ),
                'PROPERTY_MIREA_DOCREQUESTS_INSTITUTE' => CUser::GetByID($curUserID)->Fetch()['UF_DEPARTMENT'],
                'ACTIVE' => 'Y'
            );
            $arApprove = array_merge($arApprove, $this->getFormattedElements($arFilter));

            $arResult['TO_APPROVE'] = array_combine(
                array_column($arApprove, 'ID'),
                array_values($arApprove)
            );
        } else {
            $arResult['ERROR'] = Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_NO_MODULE_OPTIONS') . ' ' .
                Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_CONTACT_ADMIN');
        }

        $this->arResult = $arResult;
    }

    protected function getFormattedElements($arFilter, $arOrder = array())
    {
        $arReturn = array();

        $res = CIBlockElement::GetList(
            $arOrder ? $arOrder : array('ID' => 'DESC'),
            $arFilter
        );
        while ($ar = $res->GetNext()) {
            $arElem = array(
                'ID' => $ar['ID'],
                'DATE_CREATE' => date('d.m.Y', strtotime($ar['DATE_CREATE'])),
                'CREATED_BY' => array(
                    'ID' => $ar['CREATED_BY'],
                    'NAME' => Config::GetUserName($ar['CREATED_BY'])
                ),
                'ACTIVE' => $ar['ACTIVE'],
                'NAME' => $ar['NAME'],
                'MODIFIED_BY' => array(
                    'ID' => $ar['MODIFIED_BY'],
                    'NAME' => Config::GetUserName($ar['MODIFIED_BY'])
                ),
                'PROPERTIES' => array()
            );

            $rsProps = CIBlockElement::GetProperty($ar['IBLOCK_ID'], $ar['ID']);
            while ($arProps = $rsProps->GetNext()) {
                if ($arProps['VALUE']) {
                    if ($arProps['MULTIPLE'] == 'Y') {
                        switch ($arProps['PROPERTY_TYPE']) {
                            case 'E':
                                $arLink = CIBlockElement::GetByID($arProps['VALUE'])->GetNext();
                                $arElem['PROPERTIES'][$arProps['CODE']][] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_NAME' => $arLink['NAME'],
                                    'VALUE_PREVIEW_TEXT' => $arLink['PREVIEW_TEXT'],
                                    'VALUE_DETAIL_TEXT' => $arLink['DETAIL_TEXT']
                                );
                                break;
                            case 'L':
                                $arElem['PROPERTIES'][$arProps['CODE']][] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_LABEL' => $arProps['VALUE_ENUM'],
                                    'VALUE_CODE' => $arProps['VALUE_XML_ID']
                                );
                                break;
                            case ($arProps['PROPERTY_TYPE'] == 'S' and $arProps['USER_TYPE'] == 'employee'):
                                $arLink = CUser::GetByID($arProps['VALUE'])->GetNext();
                                $arName = array_filter(
                                    array(
                                        $arLink['LAST_NAME'],
                                        $arLink['NAME'],
                                        $arLink['SECOND_NAME']
                                    )
                                );
                                $arElem['PROPERTIES'][$arProps['CODE']][] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_NAME' => implode(' ', $arName)
                                );
                                break;
                            default:
                                $arElem['PROPERTIES'][$arProps['CODE']][] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_DESCRIPTION' => $arProps['DESCRIPTION']
                                );
                        }
                    } else {
                        switch ($arProps['PROPERTY_TYPE']) {
                            case 'E':
                                $arLink = CIBlockElement::GetByID($arProps['VALUE'])->GetNext();
                                $arElem['PROPERTIES'][$arProps['CODE']] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_NAME' => $arLink['NAME'],
                                    'VALUE_PREVIEW_TEXT' => $arLink['PREVIEW_TEXT'],
                                    'VALUE_DETAIL_TEXT' => $arLink['DETAIL_TEXT']
                                );
                                break;
                            case 'L':
                                $arElem['PROPERTIES'][$arProps['CODE']] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_LABEL' => $arProps['VALUE_ENUM'],
                                    'VALUE_CODE' => $arProps['VALUE_XML_ID']
                                );
                                break;
                            case ($arProps['PROPERTY_TYPE'] == 'S' and $arProps['USER_TYPE'] == 'employee'):
                                $arLink = CUser::GetByID($arProps['VALUE'])->GetNext();
                                $arName = array_filter(
                                    array(
                                        $arLink['LAST_NAME'],
                                        $arLink['NAME'],
                                        $arLink['SECOND_NAME']
                                    )
                                );
                                $arElem['PROPERTIES'][$arProps['CODE']] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_NAME' => implode(' ', $arName)
                                );
                                break;
                            default:
                                $arElem['PROPERTIES'][$arProps['CODE']] = array(
                                    'VALUE' => $arProps['VALUE'],
                                    'VALUE_DESCRIPTION' => $arProps['DESCRIPTION']
                                );
                        }
                    }
                } else {
                    $arElem['PROPERTIES'][$arProps['CODE']] = false;
                }
            }

            $arReturn[$ar['ID']] = $arElem;
        }

        return $arReturn;
    }
}
