<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<?php
$APPLICATION->IncludeComponent(
    'mirea.docrequests:substitution.grid',
    '',
    array(
        'SORT_DEFAULT' => array(
            'ID' => 'DESC'
        ),
    )
);
?>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

