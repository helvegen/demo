<?php

use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$arIBlockFields = array(
    'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_REQUESTS_IBLOCK_NAME')
);
