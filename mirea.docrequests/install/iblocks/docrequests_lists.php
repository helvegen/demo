<?php

use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$arIBlockFields = array(
    'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_WORK_LISTS_IBLOCK_NAME')
);
