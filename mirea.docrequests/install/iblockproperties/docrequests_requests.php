<?php

use Mirea\DocRequests\Config;
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$obConfig = new Config();

$arIBlockProps = array(
    // срок избрания
    array(
        'CODE' => 'MIREA_DOCREQUESTS_REQUEST_TIME',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_TIME_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'S',
        'MULTIPLE' => 'N'
    ),
    // комментарий при отклонении
    array(
        'CODE' => 'MIREA_DOCREQUESTS_CANCELLED_COMMENT',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_CANCELLED_COMMENT_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'S',
        'MULTIPLE' => 'N',
        'ROW_COUNT' => 5
    ),
    // доля ставки
    array(
        'CODE' => 'MIREA_DOCREQUESTS_WORK_PART',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_WORK_PART_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'N',
        'MULTIPLE' => 'N'
    ),
    // институт
    array(
        'CODE' => 'MIREA_DOCREQUESTS_INSTITUTE',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_INSTITUTE_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'N',
        'MULTIPLE' => 'N',
        'WITH_DESCRIPTION' => 'Y'
    ),
    // кафедра
    array(
        'CODE' => 'MIREA_DOCREQUESTS_DEPARTMENT',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_DEPARTMENT_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'N',
        'MULTIPLE' => 'N',
        'WITH_DESCRIPTION' => 'Y'
    ),
    // согласовано директором института
    array(
        'CODE' => 'MIREA_DOCREQUESTS_APPROVED_DIRECTOR',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_APPROVED_DIRECTOR_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'L',
        'MULTIPLE' => 'N',
        'LIST_TYPE' => 'C',
        'VALUES' => array(
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_APPROVED_YES'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_approved_director_yes'
            )
        )
    ),
    // согласовано ответственными
    array(
        'CODE' => 'MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_APPROVED_RESPONSIBLE_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'S',
        'MULTIPLE' => 'Y',
        'USER_TYPE' => 'employee',
        'WITH_DESCRIPTION' => 'Y'
    ),
    // согласовано высшим руководством
    array(
        'CODE' => 'MIREA_DOCREQUESTS_APPROVED_FINAL',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_APPROVED_FINAL_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'L',
        'MULTIPLE' => 'N',
        'LIST_TYPE' => 'C',
        'VALUES' => array(
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_APPROVED_YES'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_approved_final_yes'
            )
        )
    ),
    // статус
    array(
        'CODE' => 'MIREA_DOCREQUESTS_REQUEST_STATUS',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_STATUS_NAME'),
        'IS_REQUIRED' => 'N',
        'PROPERTY_TYPE' => 'L',
        'MULTIPLE' => 'N',
        'LIST_TYPE' => 'L',
        'VALUES' => array(
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_STATUS_APPROVE_DIRECTOR'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_status_approve_director'
            ),
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_STATUS_APPROVE_RESPONSIBLE'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_status_approve_responsible'
            ),
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_STATUS_APPROVE_FINAL'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_status_approve_final'
            ),
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_STATUS_CANCELLED'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_status_cancelled'
            ),
            array(
                'VALUE' => Loc::getMessage('MIREA_DOCREQUESTS_REQUEST_STATUS_APPROVED'),
                'XML_ID' => $obConfig->MODULE_PREFIX . '_status_approved'
            ),
        )
    )
);

// лист эффективности
if ($obConfig->moduleIBlocks['docrequests_lists']['ID']) {
    $arIBlockProps[] = array(
        'CODE' => 'MIREA_DOCREQUESTS_WORK_LIST',
        'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_WORK_LIST_NAME'),
        'IS_REQUIRED' => 'Y',
        'PROPERTY_TYPE' => 'E',
        'LINK_IBLOCK_ID' => $obConfig->moduleIBlocks['docrequests_lists']['ID']
    );
}
