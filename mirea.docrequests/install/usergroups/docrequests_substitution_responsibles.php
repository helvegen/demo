<?

use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$arGroupFields = array(
    'NAME' => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESPS_GROUP_NAME'),
    'DESCRIPTION' => Loc::getMessage('MIREA_DOCREQUESTS_SUBSTITUTION_RESPS_GROUP_DESCRIPTION'),
);
