<?php

$MESS['MIREA_DOCREQUESTS_MODULE_NAME'] = 'Запрос документов';
$MESS['MIREA_DOCREQUESTS_MODULE_DESCRIPTION'] = 'Запрос и согласование документов';
$MESS['MIREA_DOCREQUESTS_PARTNER_NAME'] = 'Хельга Хельвеген';
$MESS['MIREA_DOCREQUESTS_PARTNER_URI'] = '';
$MESS['MIREA_DOCREQUESTS_OPTIONS_ERROR'] = 'Невозможно прочитать настройки';
$MESS['MIREA_DOCREQUESTS_LISTS_IBLOCK'] = 'Список кандидатов (листов эффективности)';
$MESS['MIREA_DOCREQUESTS_REQUESTS_IBLOCK'] = 'Список заявок';
$MESS['MIREA_DOCREQUESTS_RESP_0_CALL'] = 'Способ оповещения директора института';
$MESS['MIREA_DOCREQUESTS_RESP_1_CALL'] = 'Способ оповещения ответственных';
$MESS['MIREA_DOCREQUESTS_RESP_1_USER'] = 'Ответственные';
$MESS['MIREA_DOCREQUESTS_RESP_2_CALL'] = 'Способ оповещения итогового согласующего';
$MESS['MIREA_DOCREQUESTS_RESP_2_USER'] = 'Итоговый согласующий';
$MESS['MIREA_DOCREQUESTS_INSTITUTE_MARKER'] = 'Вхождения вложенности для фильтрации подразделений';
