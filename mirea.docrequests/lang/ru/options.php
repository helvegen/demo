<?php

$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_REQUESTS_IBLOCK'] = 'Список заявок (инфоблок)';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_LISTS_IBLOCK'] = 'Список кандидатов/листов эффективности (инфоблок)';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESP_0_CALL'] = 'Способ оповещения директора института';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESP_1_USER'] = 'Ответственные (группа пользователей)';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESP_1_CALL'] = 'Способ оповещения ответственных';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESP_2_USER'] = 'Итоговый согласующий (пользователь)';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESP_2_CALL'] = 'Способ оповещения итогового согласующего';
$MESS['MIREA_DOCREQUESTS_FORBIDDEN'] = 'Нет доступа';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_SETTINGS'] = 'Замещение вакантной должности';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_EMAIL'] = 'Email';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_PUSH'] = 'Уведомление на портале';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_NO_CALL'] = 'Не оповещать';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_NOT_SELECTED'] = 'Не выбрано';
$MESS['MIREA_DOCREQUESTS_INSTITUTION_MARKER'] = 'Вхождения вложенности для фильтрации подразделений';
$MESS['MIREA_DOCREQUESTS_INSTITUTION_MARKER_NOTE'] = '
<sup>1</sup> - В этом поле перечислите через запятую, подразделения какого типа обязательно должны входить в состав института
(например, "кафедра, комиссия"). Эти значения будут использованы, чтобы отличить институт от другого подразделения.';
