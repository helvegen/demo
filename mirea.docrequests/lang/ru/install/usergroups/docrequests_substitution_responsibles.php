<?

$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESPS_GROUP_NAME'] = 'Ответственные за заявки на замещение вакантных должностей';
$MESS['MIREA_DOCREQUESTS_SUBSTITUTION_RESPS_GROUP_DESCRIPTION'] = 'Второй уровень согласования заявки на замещение вакантной должности. Все пользователи группы обязаны согласовать заявку.';
